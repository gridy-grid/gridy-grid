

export var EL_SHARED_ATTRS = {
    'theme': 'theme', 'base-path': 'basePath', 'use-shadow-root': 'useShadowRoot', 'lang': 'lang'
};

export var LANGS_BY_CODES = {
    'en_US': 'EN',
    'en_UK': 'EN',
    'en': 'EN',
    'EN': 'EN',
    'us': 'EN',
    'ru_RU': 'RU',
    'ru': 'RU',
    'cn': 'CN',
    'CN': 'CN',
    'zh': 'CN',
    'de': 'DE',
    'DE': 'DE'
};

export class GridyConfig extends HTMLElement {

    configureElement(el) {
        for (let attrName of Object.keys(EL_SHARED_ATTRS)) {
            let value = el.getAttribute(attrName);
            if (value === null && this.getAttribute(attrName) !== null) {
                el.setAttribute(attrName, this.getAttribute(attrName));
            }
        }
    }

    reconfigureElement(el, detail) {
        if (detail) {
            console.log(`el attribute ${detail.name} changed from: ${detail.oldValue} to: ${detail.newValue}`);
            el.setAttribute(detail.name, detail.newValue);
        } else {
            for (let attrName of Object.keys(EL_SHARED_ATTRS)) {
                let value = el.getAttribute(attrName);
                if (this.getAttribute(attrName) !== null && value !== this.getAttribute(attrName)) {
                    el.setAttribute(attrName, this.getAttribute(attrName));
                }
            }
        }
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (EL_SHARED_ATTRS[name] && oldValue !== newValue) {
            this.dispatchEvent(
                new CustomEvent('configChanged',
                    { detail: { name: name, oldValue: oldValue, newValue: newValue }}
            ));
        }
    }

    static get observedAttributes() {
        return Object.keys(EL_SHARED_ATTRS);
    }
}


import { DataSourceLocal } from "./data-source-local.js";

import { XmlHttpClient } from "../../../sk-core/complets/http/xml-http-client.js";
import { FetchHttpClient } from "../../../sk-core/complets/http/fetch-http-client.js";
import { BACK_PAGED_AN, DATA_PATH_AN, URL_AN } from "./gridy-data-source.js";
import { StringUtils } from "../../../sk-core/complets/string-utils.js";
import { REQ_TYPE_GET } from "../../../sk-core/complets/http/http-client-base.js";
import { ResLoader } from "../../../sk-core/complets/res-loader.js";
import { DATA_FETCH_START_EVT, DataFetchStartEvent } from "./event/data-fetch-start-event.js";
import { DataFetchEndEvent } from "./event/data-fetch-end-event.js";

export const REQ_PARAM_MAP_AN = "req-param-map";
export const REQ_TYPE_AN = "req-type";
export const REQ_HEADERS_AN = "req-headers";

export const REQ_PARAM_TYPE_BODY = "body";

export const RESP_PARAM_MAP_AN = "resp-param-map";

export const HTTP_CLIENT_TYPE_AN = "http-client-type";
export const HTTP_CLIENT_CLASS_NAME_AN = "http-client-cn";
export const HTTP_CLIENT_CLASS_PATH_AN = "http-client-path";
export const HTTP_CLIENT_OPTIONS_AN = "http-client-options";

export const TOTAL_RESULTS_PN = "totalResults";
export const SORT_FIELD_PN = "sortField";
export const SORT_DIRECTION_PN = "sortDirection";
export const PAGE_PN = "page";
export const PER_PAGE_PN = "perPage";

export class DataSourceAjax extends DataSourceLocal {

    //url: string;
    //method: string;

    //backPaged: boolean;
    //loadOnInit: boolean;

    //http;
    //auth;

    //data: [];

    constructor() {
        super();
        this.loadOnInit = true;
        this.loadDate = -1;
    }
    
    confFromEl(comp) {
        this.logger.debug('DataSourceAjax.confFromEl()', arguments);
        if (comp.hasAttribute(URL_AN)) {
            this.url = comp.getAttribute(URL_AN);
        }
        if (comp.hasAttribute(DATA_PATH_AN) || this.comp.hasAttribute('datapath')) {
            this.dataPath = comp.hasAttribute(DATA_PATH_AN) ? comp.getAttribute(DATA_PATH_AN) : this.comp.hasAttribute('datapath');
        }
        if (comp.hasAttribute(BACK_PAGED_AN)) {
            this.backPaged = comp.getAttribute(BACK_PAGED_AN) !== 'false';
        }
        if (comp.hasAttribute(REQ_PARAM_MAP_AN)) {
            try {
                this.reqParamMap = JSON.parse(comp.getAttribute(REQ_PARAM_MAP_AN));
            } catch (e) {
                this.logger.warn(`Error parsing gridy-data-source ${REQ_PARAM_MAP_AN} attr to json ${comp.getAttribute(REQ_PARAM_MAP_AN)}`);
            }
        }
        if (comp.hasAttribute(RESP_PARAM_MAP_AN)) {
            try {
                this.respParamMap = JSON.parse(comp.getAttribute(RESP_PARAM_MAP_AN));
            } catch (e) {
                this.logger.warn(`Error parsing gridy-data-source ${RESP_PARAM_MAP_AN} attr to json ${comp.getAttribute(RESP_PARAM_MAP_AN)}`);
            }
        }
        if (comp.hasAttribute(REQ_TYPE_AN)) {
            this.reqType = comp.getAttribute(REQ_TYPE_AN);
        }
        if (comp.hasAttribute(REQ_HEADERS_AN)) {
            try {
                this.reqHeaders = JSON.parse(comp.getAttribute(REQ_HEADERS_AN));
            } catch (e) {
                this.logger.warn(`Error parsing gridy-data-source ${REQ_HEADERS_AN} attr to json ${comp.getAttribute(REQ_PARAM_MAP_AN)}`);
            }
        }
        if (comp.hasAttribute(HTTP_CLIENT_CLASS_NAME_AN)) {
            this.httpClassName = comp.getAttribute(HTTP_CLIENT_CLASS_NAME_AN);
        }
        if (comp.hasAttribute(HTTP_CLIENT_CLASS_PATH_AN)) {
            this.httpClassPath = comp.getAttribute(HTTP_CLIENT_CLASS_PATH_AN);
        }
        if (comp.hasAttribute(HTTP_CLIENT_TYPE_AN)) {
            this.httpClientType = comp.getAttribute(HTTP_CLIENT_TYPE_AN);
        }
        if (comp.hasAttribute(HTTP_CLIENT_OPTIONS_AN)) {
            try {
                this.httpClientOptions = JSON.parse(comp.getAttribute(HTTP_CLIENT_OPTIONS_AN));
            } catch (e) {
                this.logger.warn(`${HTTP_CLIENT_OPTIONS_AN} attribute of data-source-ajax element is not valid`);
            }
        }
        ResLoader.cacheClassDefs(XmlHttpClient, FetchHttpClient);
        this.http = ResLoader.dynLoad(this.httpClassName, this.httpClassPath, function(def) { // load is sync, custom path not supported here
                return this.httpClientOptions ? new def(this.httpClientOptions) : new def();
            }.bind(this), true, false, false ,false, false, false, true,
            true, window[this.httpClassName]);
    }
    
    get httpClientType() {
        if (! this._httpClientType) {
            this._httpClientType = 'xml';
        }
        return this._httpClientType;
    }
    
    set httpClientType(clientType) {
        this._httpClientType = clientType;
    }
    
    get httpClassName() {
        if (! this._httpClassName) {
            if (this.httpClientType) {
                this._httpClassName = `${StringUtils.capitalizeFirstLetter(this.httpClientType)}HttpClient`;
            } else {
                this._httpClassName = "XmlHttpClient";
            }
        }
        return this._httpClassName;
    }
    
    get httpClassPath() {
        if (! this._httpClassPath) {
            this._httpClassPath = `/node_modules/sk-core/complets/${this.httpClientType}-http-client.js`;
        }
        return this._httpClassPath;
    }

    init(params) {
        this.logger.debug('DataSourceAjax.init()', arguments);
        return new Promise(function(resolve, reject) {
            this.http = this.http ? this.http : new XmlHttpClient();
            if (this.auth) {
                this.http.auth = this.auth;
            }
            if (this.loadOnInit) {
                this.fetchData(params).then((detail) => {
                    this.loadData(null, detail);
                    resolve();
                });
            }
        }.bind(this))
    }
    
    filter(values, fieldTitle, mode, params) {
        params = Object.assign({ values: values, fieldTitle: fieldTitle, mode: mode }, params);
        this.fetchData(params).then((detail) => {
            this.loadData(null, detail);
        });
    }
    
    get reqParamSep() {
        if (! this._reqParamSep) {
            this._reqParamSep = '&';
        }
        return this._reqParamSep;
    }
    
    set reqParamSep(sep) {
        this._reqParamSep = sep;
    }
    
    get reqType() {
        if (! this._reqType) {
            this._reqType = REQ_TYPE_GET;
        }
        return this._reqType;
    }
    
    set reqType(reqType) {
        this._reqType = reqType;
    }
    
    get contentType() {
        return this._contentType;
    }
    
    set contentType(contentType) {
        this._contentType = contentType;
    }
    
    get reqHeaders() {
        return this._reqHeaders;
    }
    
    set reqHeaders(reqHeaders) {
        this._reqHeaders = reqHeaders;
    }
    
    get respParamMap() {
        if (! this._respParamMap) {
            this._respParamMap = {};
        }
        return this._respParamMap;
    }
    
    set respParamMap(respParamMap) {
        this._respParamMap = respParamMap;
    }
    
    addReqParam(param, value, addSeparator = true) {
        if (this.reqParamMap) {
            if (this.reqParamMap[param]) {
                if (typeof this.reqParamMap[param] === 'string') {
                    return (addSeparator ? this.reqParamSep : '') + this.reqParamMap[param] + '=' + value;
                } else {
                    let cfg = this.reqParamMap[param];
                    if (cfg.type === REQ_PARAM_TYPE_BODY) {
                        return '';
                    }
                }
            }
        }
        return (addSeparator ? this.reqParamSep : '') + param + '=' + value;
    }

    buildUrl(params) {
        this.logger.debug('DataSourceLocal.buildUrl()', arguments);
        let page = params.page || 1;
        let perPage = params.perPage || 10;
        if (params.backPaged) {
            let url = this.url + '?' + this.addReqParam(PAGE_PN, page, false) + this.addReqParam(PER_PAGE_PN, perPage);
            if (params.sortField) {
                url += this.addReqParam(SORT_FIELD_PN, params.sortField) + this.addReqParam(SORT_DIRECTION_PN, params.sortDirection);
            }
            if (params.values) {
                let filterId = 0;
                for (let value of params.values) {
                    for (let key of Object.keys(value)) {
                        url += this.addReqParam(`filter_${filterId}_${key}`, `${value[key]}`);
                    }
                }
            }
            return url;
        } else {
            return this.url;
        }
    }
    
    buildBody(params) {
        this.logger.debug('DataSourceLocal.buildBody()', arguments);
        let body = {};
        for (let param of Object.keys(params)) {
            if (this.reqParamMap) {
                let cfg = this.reqParamMap[param];
                if (cfg) {
                    if (typeof cfg === 'object') {
                        if (cfg.type === REQ_PARAM_TYPE_BODY) {
                            body[cfg.name] = params[param];
                        }
                    }
                }
            }
        }
        if (this.contentType && this.contentType.match('application/json')) {
            return JSON.stringify(body);
        } else {
            return Object.keys(body).reduce(function(a,k){a.push(k+'='+encodeURIComponent(body[k]));return a},[]).join('&');
        }
    }
    
    get loadPromises() {
        if (! this._loadPromises) {
            this._loadPromises = new Map();
        }
        return this._loadPromises;
    }
    
    prepareParams(params) {
        if (params.values) {
            for (let value of params.values) {
                delete(value.ctx);
                delete(value.comparator);
            }
        }
        return params;
    }
    
    getRespParam(name, response) {
        let alias = this.respParamMap[name];
        if (alias) {
            if (alias.startsWith('$')) {
                let result = this.jsonPath.query(response, alias);
                return result[0];
            } else {
                return response[alias];
            }
        } else {
            return response[name];
        }
    }

    fetchData(params) {
        this.logger.debug('DataSourceLocal.fetchData()', arguments);
        params = params || {};
        if (this.backPaged) {
            params = this.prepareParams(params);
            params.backPaged = true;
        }
        let promiseKey = this.hashDataItem(params);
        if (! this.loadPromises.get(promiseKey)) {
            this.loadPromises.set(promiseKey, new Promise((resolve, reject) => {
                if (!this.url) {
                    reject("No data url provided");
                }
                this.dispatchEvent(new DataFetchStartEvent({
                    detail: params
                }));
                let onResponse = this.http.request(this.buildUrl(params), this.reqType, this.contentType, this.buildBody(params), this.reqHeaders);
                onResponse.then((request) => {
                    this.dispatchEvent(new DataFetchEndEvent({
                        detail: params
                    }));
                    this.dispatchEvent(new CustomEvent('dataFetched', {
                        detail: params
                    }));
                    let response = request.response ? request.response : request;
                    let detail = {
                        response: response,
                        backPaged: params.backPaged,
                        totalResults: this.getRespParam(TOTAL_RESULTS_PN, response) || this.data.length
                    };
                    let respSortField = this.getRespParam(SORT_FIELD_PN, response);
                    let respSortDirection = this.getRespParam(SORT_DIRECTION_PN, response);
                    if (params[SORT_FIELD_PN] || respSortField) {
                        detail[SORT_FIELD_PN] = respSortField || params[SORT_FIELD_PN];
                        detail[SORT_DIRECTION_PN] = respSortDirection || params[SORT_DIRECTION_PN];
                    }
                    //this.data = response;

                    resolve(detail);
                });
            }));
        }
        return this.loadPromises.get(promiseKey);
    }


}
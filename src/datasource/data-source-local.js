
import { JsonPath, JSONPATH_CN, JSONPATH_PT } from "../../../sk-core/src/json-path.js";
import { ConsoleLogger } from "../../../sk-core/complets/log.js";
import { SkNumberParser } from "../../../sk-core/src/locale/sk-number-parser.js"
import { ResLoader } from "../../../sk-core/complets/res-loader.js";
import { DataLoadEvent } from "./event/data-load-event.js";
import { DataLoadStartEvent } from "./event/data-load-start-event.js";
import { DataClearEvent } from "./event/data-clear-event.js";
import { ObjectUtils } from "../../../sk-core/complets/object-utils.js";

export const FILTER_MODE_LESS = "lt";
export const FILTER_MODE_INCL_LESS = "le";
export const FILTER_MODE_GREATER = "gt";
export const FILTER_MODE_INCL_GREATER = "ge";
export const FILTER_MODE_NOT_EQUALS = "ne";
export const FILTER_MODE_CONTAINS = "cntn";
export const FILTER_MODE_EQUALS = "eq";


export class DataSourceLocal extends EventTarget {

    //data: [];
    //loadTimest: number;
    //dataPath: string; // jsonpath for data rows root;

    constructor() {
        super();
        this.jsonPath;
        this.loadOnInit = true;
        this.loadDate = -1;
        this.meta = { rows: new Map()};
    }

    init() {
		return Promise.resolve();
    }

    get logger() {
        if (! this._logger) {
            this._logger = ConsoleLogger.instance();
        }
        return this._logger;
    }

    set logger(logger) {
        this._logger = logger;
    }
	
	get jsonPath() {
		if (! this._jsonPath) {
			this._jsonPath = ResLoader.dynLoad(JSONPATH_CN, JSONPATH_PT, function(def) {
				return new def();
			}.bind(this), false, false, false ,false, false, false, true, true, JsonPath);
		}
		return this._jsonPath;
	}

    set jsonPath(jsonPath) {
        this._jsonPath = jsonPath;
    }
    
	get numberParserCn() {
        return 'SkNumberParser';
    }
    
    get numberParserPath() {
        return '/node_modules/sk-core/src/locale/sk-number-parser.js';
    }
    
    numberParser(lang) {
        if (! this._numberParser) {
			if (! window.SkNumberParser) {
				window.SkNumberParser = SkNumberParser;
			}            
            this._numberParser = 
                ResLoader.dynLoad(this.numberParserCn, this.numberParserPath, function(def) {
                    return new def(lang);
                }.bind(this), false, false, false ,false, false, false, true, true);
        
        } 
        return this._numberParser; 
    }
    

    loadData(data, params) {
        this.logger.debug('DataSourceLocal.loadData', arguments);
        this.dispatchEvent(new DataLoadStartEvent({
            detail: params
        }));
        params = params || {};
        let dataRows = null;
        if (!data && params.response && this.dataPath) {
            let onValue = this.jsonPath.query(params.response, this.dataPath);
            dataRows = onValue[0];
        } else {
			if (data) {
				dataRows = data;
			} else {
				dataRows = params.data || params.response;
			}
        }
        this.data = params.data = [...dataRows];
        this.loadTimest = Date.now();
        this.dispatchEvent(new DataLoadEvent({
            detail: params
        }));
    }
	
	clearData() {
		this.data = [];
		this.dispatchEvent(new DataClearEvent({

		}));
	}

    get total() {
        return this.dataFiltered ? this.dataFiltered.length : this.data.length;
    }

    get data() {
        return this.dataFiltered || this._data || [];
    }

    set data(data) {
        this._data = data;
    }
    
    get fieldsByTitle() {
        if (! this._fieldsByTitle) {
            this._fieldsByTitle = {};
        }
        return this._fieldsByTitle;
    }

    sort(field, direction) {
        this.logger.debug('DataSourceLocal.sort', arguments);
        let sortFunc = (a, b) => {
            let aVal = null;
            let bVal = null;
            if (field) {
	            if (field.startsWith('$')) {
		            let aOnValue = this.jsonPath.query(a, field);
		            aVal = aOnValue[0];
		            let bOnValue = this.jsonPath.query(b, field);
		            bVal = bOnValue[0];
	            } else {
					let fieldCfg = this.findField(field);
					if (fieldCfg.calc) {
						let aResult = this.calcRow(fieldCfg.calc, a)
						let aKeys = typeof aResult === 'object' ? Object.keys(aResult) : null;
						aVal = aKeys ? aResult[aKeys[0]] : aResult;
						let bResult = this.calcRow(fieldCfg.calc, b);
						let bKeys = typeof bResult === 'object' ? Object.keys(bResult) : null;
						bVal = bKeys ? bResult[bKeys[0]] : bResult;
					} else {
						aVal = a[field];
						bVal = b[field];
					}
	            }
            } else {
				aVal = a;
				bVal = b;
            }
            if (aVal === bVal) {
                return 0;
            }
            if (direction === undefined || direction === 'asc') {
                if (typeof aVal === 'string') {
                    return aVal.localeCompare(bVal);
                } else {
                    return aVal - bVal;
                }
            } else {
                if (typeof aVal === 'string') {
                    return bVal.localeCompare(aVal);
                } else {
                    return bVal - aVal;
                }
            }

        };
        this.data.sort(sortFunc);
        if (this.dataFiltered) {
            this.dataFiltered.sort(sortFunc);
        }
    }

    fieldByTitle(title, fields) {
	    if (! fields) {
		    fields = this.fields;
	    }
        if (this.fieldsByTitle[title]) {
            return this.fieldsByTitle[title];
        }
        for (let field of fields) {
            if (field.title === title) {
                this.fieldsByTitle[title] = field;
                return field;
            }
        }
        return null;
    }
	
	fieldByPath(path, fields) {
		if (! fields) {
			fields = this.fields;
		}
		for (let field of fields) {
			if (field.path === path) {
				return field;
			}
		}
		return null;
	}
	
	findField(searchVal, fields) {
		if (! fields) {
			fields = this.fields;
		}
		for (let field of fields) {
			if (field.path === searchVal || field.title === searchVal || field.name === searchVal) {
				return field;
			}
		}
		return null;
	}
    
    guessNumber(value, ctx) {
		if (ctx && ctx.locale && ctx.locale.lang && ctx.locale.lang !== 'EN') {
			value = this.numberParser(ctx.locale.lang).parse(value);
		} 
		if (! isNaN(value)) {
			value = parseFloat(value);
		}
		return value;
	}
    
    compareDataItem(dataItemValue, value, mode, comparator, ctx) {
		if (typeof comparator === 'function') {
			if (comparator(dataItemValue, value, mode, ctx)) {
				return true;
			}		
		} else {
			if (value === "") {
				return true;
			}
			if (mode === FILTER_MODE_EQUALS) {
				if (dataItemValue === value) {
					return true;
				}
			} else if (mode === FILTER_MODE_NOT_EQUALS) {
				if (dataItemValue !== value) {
					return true;
				}				
			} else if (mode === null || mode === undefined || mode === FILTER_MODE_CONTAINS) { 
				if (dataItemValue && (dataItemValue).toString().indexOf(value) >= 0) {
					return true;
				}
			} else {
				value = this.guessNumber(value, ctx);
				dataItemValue = this.guessNumber(dataItemValue, ctx);
				if (mode === FILTER_MODE_GREATER) { 
					if (dataItemValue > value) {
						return true;
					}
				} else if (mode === FILTER_MODE_INCL_GREATER) {	
					if (dataItemValue >= value) {
						return true;
					}
				} else if (mode === FILTER_MODE_INCL_LESS) {	
					if (dataItemValue <= value) {
						return true;
					}					
				} else if (mode === FILTER_MODE_LESS) {
					if (dataItemValue < value) {
						return true;
					}
				} 
			}
		}
        
    }
    
    matchDataItem(dataItem, field, value, mode, comparator, ctx) {
        if (! isNaN(mode)) {
            mode = parseFloat(mode);
        }
        if (typeof field == 'string') {
            if (Array.isArray(value)) {
                for (let val of value) {
                    if (this.compareDataItem(dataItem[field], val, mode, comparator, ctx)) {
                        return true;
                    }
                }
            } else {
                if (this.compareDataItem(dataItem[field], value, mode, comparator, ctx)) {
                    return true;
                }
            }
        } else {
            let onResult = this.jsonPath.query(dataItem, field.path);
            let result = onResult[0];
            if (result !== null) {
                let itemValue = (result).toString();
                if (Array.isArray(value)) {
                    for (let val of value) {
                        if (this.compareDataItem(itemValue, val, mode, comparator, ctx)) {
                            return true;
                        }
                    }
                } else {
                    if (this.compareDataItem(itemValue, value, mode, comparator, ctx)) {
                        return true;
                    }
                }
            }
        }
    }
	
	hashDataItem(item) {
		return ObjectUtils.hashCode(item);
	}

    /**
     * value - value to filter
     * fieldTitle - title defined in datasource for field selection
     * mode - g (greater),l (lower), default - equals
     * */
    filter(value, fieldTitle, mode) {
        this.logger.debug('DataSourceLocal.filter', arguments);
        let field = null;
        if (fieldTitle) {
            field = this.fieldByTitle(fieldTitle, this.fields);
        }
        this.dataFiltered = this._data.filter((dataItem) => {
            if (field) {
                if (this.matchDataItem(dataItem, field, value, mode)) {
                    return true;
                }
            } else {
                if (Array.isArray(value) && value.length >= 1) {
					let cumVal = true;
					for (let val of value) {
						if (val.title && val.mode) {						
							let field = this.fieldByTitle(val.title, this.fields);
						 
							if (! this.matchDataItem(dataItem, field, val.value, val.mode, val.comparator, val.ctx)) {
								cumVal = false;
							}
						} else {
							let localCumVal = false;
		                    for (let field of this.fields) {
								if (this.matchDataItem(dataItem, field, val, mode)) {
									localCumVal = true;
								}
							}
							if (! localCumVal) {
								cumVal = false;
							}
						}
					}
					if (cumVal) {
						return cumVal;
					}
                } else {
                    for (let field of this.fields) {
                        if (this.matchDataItem(dataItem, field, value, mode)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        });
    }
    
	calcRow(rules, dataItem) {
		if (typeof rules === 'object') {
			let results = {};
			for (let ruleKey of Object.keys(rules)) {
				try {
					results[ruleKey] = (Function(rules[ruleKey])).call(this, dataItem);
				} catch {
					this.logger.warn(`Error calculating rule ${ruleKey} -> ${rules[ruleKey]} for row ${JSON.stringify(dataItem)}`);
				}
			}
			return results;
		} else {
			try {
				let result = (Function(rules)).call(this, dataItem);
				return result;
			} catch {
				this.logger.warn(`Error calculating rule ${ruleKey} -> ${rules[ruleKey]} for row ${JSON.stringify(dataItem)}`);
			}
		}
	}
}


import { DataSourceLocal } from './data-source-local.js';
import { DataSourceAjax } from './data-source-ajax.js';

import { Renderer } from "../../../sk-core/complets/renderer.js";
import { JsonPath } from "../../../sk-core/src/json-path.js";

import { SkRenderEvent } from "../../../sk-core/src/event/sk-render-event.js";
import { ResLoader } from "../../../sk-core/complets/res-loader.js";
import { StringUtils } from "../../../sk-core/complets/string-utils.js";

export const DATA_SOURCE_TN = "gridy-data-source";
export const DATA_FIELD_TN = "gridy-data-field";
export const DATA_SOURCE_TYPE_AN = "datasource-type";
export const URL_AN = "url";
export const DATA_PATH_AN = "data-path";
export const BACK_PAGED_AN = "back-paged";
export const FIELDS_AN = "fields";
export const DATA_REF_AN = "data-ref";

export class GridyDataSource extends HTMLElement {

    get fields() {
        return this.getAttribute(FIELDS_AN);
    }
    
    get dsType() {
        return this.hasAttribute(DATA_SOURCE_TYPE_AN) ?
            this.getAttribute(DATA_SOURCE_TYPE_AN) : 'local';
    }
    
    get dsClassName() {
        // type can be specified as full class name if name started with DataSource, otherwise assuming this is just a suffix
        return this.dsType.indexOf('DataSource') > -1 ? this.dsType : `DataSource${StringUtils.capitalizeFirstLetter(this.dsType)}`;
    }
    
    get dsPath() {
        return `/node_modules/gridy-grid/src/data-source-${this.dsType}.js`;
    }
    
    get dataSource() {
        if (! this._dataSource) {
            ResLoader.cacheClassDefs(DataSourceLocal, DataSourceAjax);
            this._dataSource = ResLoader.dynLoad(this.dsClassName, this.dsPath, function(def) { // load is sync, custom path not supported here
                return new def();
            }.bind(this), true, false, false ,false, false, false, true,
                true, window[this.dsClassName]);
            if (typeof this._dataSource.confFromEl === 'function') {
                this._dataSource.confFromEl(this);
            }
        }
        return this._dataSource;
        
    }

    config() {
        if (! this.dataSource.fields) {
            if (this.getAttribute(FIELDS_AN)) {
                this.fieldsFromAttr();
            } else {
                this.fieldsFromML();
            }
        }
        this.setAttribute(DATA_SOURCE_TYPE_AN, this.dataSource.constructor.name);
    }
    
    fieldsFromAttr() {
        this.dataSource.fields = JSON.parse(this.getAttribute(FIELDS_AN));
    }
    
    fieldsFromML() {
        let fieldEls = this.querySelectorAll(DATA_FIELD_TN);
        let fields = [];
        for (let fieldEl of fieldEls) {
            let field = {};
            for (let attrName of fieldEl.attributes) {
                let propName = StringUtils.css2CamelCase(attrName.name);
                let value = fieldEl.getAttribute(attrName.name);
                if (value) {
                    field[propName] = value;
                }
            }
            fields.push(field);
        }
        this.dataSource.fields = fields;
    }

    loadDataRef() {
        let dataRef = this.hasAttribute(DATA_REF_AN) ? this.getAttribute(DATA_REF_AN) : this.getAttribute('dataref');
        if (typeof window[dataRef] === 'function') {
            window[dataRef].call(this);
        } else if (dataRef && window[dataRef]) {
            this.dataSource.loadData(window[dataRef]);
        }
    }
    
    whenRendered(callback) {
        return Renderer.callWhenRendered(this, callback);
    }

    connectedCallback() {
        this.config();
        this.dispatchEvent(new SkRenderEvent({ bubbles: true, composed: true }));
    }
}

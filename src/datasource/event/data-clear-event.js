

export const DATA_CLEAR_EVT = 'dataclear';

export class DataClearEvent extends CustomEvent {
    constructor(options) {
        super(DATA_CLEAR_EVT, options);
    }
}

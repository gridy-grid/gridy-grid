

export const DATA_LOAD_START_EVT = 'dataloadstart';

export class DataLoadStartEvent extends CustomEvent {
    constructor(options) {
        super(DATA_LOAD_START_EVT, options);
    }
}

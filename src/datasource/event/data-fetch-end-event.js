

export const DATA_FETCH_END_EVT = 'datafetchend';

export class DataFetchEndEvent extends CustomEvent {
    constructor(options) {
        super(DATA_FETCH_END_EVT, options);
    }
}



export const DATA_FETCH_START_EVT = 'datafetchstart';

export class DataFetchStartEvent extends CustomEvent {
    constructor(options) {
        super(DATA_FETCH_START_EVT, options);
    }
}

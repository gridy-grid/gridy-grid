

export const DATA_LOAD_EVT = 'dataload';

export class DataLoadEvent extends CustomEvent {
    constructor(options) {
        super(DATA_LOAD_EVT, options);
    }
}



export const ITEM_SELECT_EVT = 'itemselect';

export class ItemSelectEvent extends CustomEvent {
    constructor(options) {
        super(ITEM_SELECT_EVT, options);
    }
}

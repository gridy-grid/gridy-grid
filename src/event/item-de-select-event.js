

export const ITEM_DESELECT_EVT = 'itemdeselect';

export class ItemDeSelectEvent extends CustomEvent {
    constructor(options) {
        super(ITEM_DESELECT_EVT, options);
    }
}


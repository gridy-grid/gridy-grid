

export const BOOTSTRAP_EVT = 'bootstrap';

export class BootstrapEvent extends CustomEvent {
    constructor(options) {
        super(BOOTSTRAP_EVT, options);
    }
}

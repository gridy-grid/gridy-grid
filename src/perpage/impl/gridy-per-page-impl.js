
import { TPL_PATH_AN } from "../../../../sk-core/src/sk-element.js";
import { SkComponentImpl } from "../../../../sk-core/src/impl/sk-component-impl.js";

export class GridyPerPageImpl extends SkComponentImpl {

    get tplPath() {
        if (! this._tplPath) {
            this._tplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'per-page.tpl.html';
        }
        return this._tplPath;
    }

    renderImpl() {
        this.logger.debug('GridyPerPageImpl.renderImpl', arguments);
        this.comp.bootstrap();
    }

    renderOptions() {
        for (let option of this.comp.perPageOptions) {
            if (option === this.comp.perPage) {
                this.comp.selectEl.insertAdjacentHTML('beforeend', `<option selected="selected" value=${option}>${option}</option>`);
            } else {
                this.comp.selectEl.insertAdjacentHTML('beforeend', `<option value=${option}>${option}</option>`);
            }
        }
    }
}



import { Renderer } from '../../../sk-core/complets/renderer.js';

import { ATTR_PLUGINS_AN, TPL_PATH_AN, IMPL_PATH_AN } from '../../../sk-core/src/sk-element.js';

import { DS_REF_AN, GridyElement, PER_PAGE_AN, PER_PAGE_DEFAULT } from "../gridy-element.js";
import { PerPageChangeEvent } from "./event/per-page-change-event.js";
import { DATA_LOAD_EVT } from "../datasource/event/data-load-event.js";


export const PER_PAGE_OPTIONS_DEFAULT = [10, 25, 50, 100];

export class GridyPerPage extends GridyElement {

    //renderer: Renderer;
    //theme: string;

    //tpl;

    //perPage: number;

    //tplPath;
    //tpl;

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    get tplPath() {
        return this.getAttribute(TPL_PATH_AN) || this.impl.tplPath;
    }

    set tplPath(tplPath) {
        this.setAttribute(TPL_PATH_AN, tplPath);
    }

    get implClassName() {
        return `GridyPerPage${this.capitalizeFirstLetter(this.theme)}`;
    }

    get implPath() {
        return this.getAttribute(IMPL_PATH_AN) ||
            `/node_modules/gridy-grid-${this.theme}/src/gridy-per-page-${this.theme}.js`;
    }
    
    set implPath(path) {
        this.setAttribute(IMPL_PATH_AN, path);
    }

    onchange(event) {
        this.logger.debug('GridyPerPage.onchange()', arguments);
        // :TODO get select value backward compatible
        let perPage = parseInt(this.selectEl.value);
        this.perPage = perPage;
        this.dispatchEvent(new PerPageChangeEvent({ detail: { perPage: perPage }}));
    }

    get perPage() {
        if (! this._perPage) {
            let perPage = PER_PAGE_DEFAULT;
            if (this.hasAttribute(PER_PAGE_AN)) {
                perPage = parseInt(this.getAttribute(PER_PAGE_AN));
            } else {
                if (this.hasAttribute('per-page-default')) {
                    perPage = parseInt(this.getAttribute('per-page-default'));
                }
            }
            if (this.perPageOptions.indexOf(perPage) > 0) {
                this._perPage = perPage;
            } else {
                this._perPage = this.perPageOptions[0];
            }
        }
        return this._perPage;
    }

    set perPage(perPage) {
        this._perPage = parseInt(perPage);
        this.setAttribute(PER_PAGE_AN, perPage);
    }

    get perPageOptions() {
        if (! this._perPageOptions) {
            this._perPageOptions = this.hasAttribute('per-page-options')
                ? JSON.parse(this.getAttribute('per-page-options')) : PER_PAGE_OPTIONS_DEFAULT;
        }
        return this._perPageOptions;
    }
    
    get dsRef() {
        return this.hasAttribute(DS_REF_AN) ? this.getAttribute(DS_REF_AN) : this.getAttribute('dsref');
    }
    
    set dsRef(dsRef) {
        this.setAttribute(DS_REF_AN, dsRef);
    }

    get selectEl() {
        return this.querySelector('select');
    }

    bindDataSource(dataSource) {
        this.logger.debug('GridyPerPage.bindDataSource()', arguments);
        if (dataSource) {
            this.dataSource = dataSource;
        }
        this.dataSource.addEventListener(DATA_LOAD_EVT, (event) => {
            if (event.detail.totalResults) {
            }
            this.finishRender();
        });
    }
    
    bindChange() {
        if (this.changeHandler) {
            this.selectEl.removeEventListener('change', this.changeHandler);
        }
        this.changeHandler = this.onchange.bind(this);
        this.selectEl.addEventListener('change', this.changeHandler);
    }

    bootstrap() {
        super.bootstrap();
        this.logger.debug('GridyPerPage.bootstrap()', arguments);
        if (! this.dataSource) {
            if (this.dsRef && window[this.dsRef]) {
                if (typeof window[this.dsRef] === 'function') {
                    window[this.dsRef].call(this);
                } else {
                    let dsEl = window[this.dsRef];
                    this.dataSource = dsEl.dataSource;
                }
            } else {
                if (this.dsRef && typeof this.dsRef === 'string' && this.dsRef.indexOf('.') > 0) {
                    let pathTokens = this.dsRef.split('.');
                    let root = window[pathTokens[0]];
                    if (root) {
                        let ds = root[pathTokens[1]];
                        if (ds) {
                            this.dataSource = ds;
                        }
                    }
                }
            }
        }
        if (this.dataSource) {
            this.bindDataSource();
            if (this.dataSource.total > 0) {

            }
        }
        if (this.basePath) {
            let context = this;
            this.renderer.mountTemplate(this.tplPath, 'perPageTpl', this, { ...context, ...this.tplVars }).then((templateEl) => {
                this.tpl = templateEl;
                this.innerHTML = '';
                this.appendChild(this.renderer.prepareTemplate(this.tpl));
                this.impl.renderOptions();
                this.bindChange();
                this.finishRender();
            });
        }
    }

    connectedCallback() {
        this.logger.debug('GridyPerPage.connectedCallback()', arguments);

        this.useConfigEl();
        Renderer.configureForElement(this);
        let attrPlugins = this.confValOrDefault(ATTR_PLUGINS_AN, false);
        if (attrPlugins) {
            this.bindAttrPlugins();
        }
        if (this.impl) { // if impl was cached render it
            this.impl.renderImpl();
        }
    }

    changePerPage(perPage) {
        this.selectEl.innerHTML = '';
        this.impl.renderOptions();
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === PER_PAGE_AN && oldValue !== newValue) {
            this.changePerPage(newValue);
        }
    }

    static get observedAttributes() {
        return [ PER_PAGE_AN ];
    }
}

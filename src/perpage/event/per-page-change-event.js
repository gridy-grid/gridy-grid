

export const PER_PAGE_CHANGE_EVT = 'perpagechange';

export class PerPageChangeEvent extends CustomEvent {
    constructor(options) {
        super(PER_PAGE_CHANGE_EVT, options);
    }
}

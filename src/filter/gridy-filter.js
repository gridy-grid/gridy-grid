
import { Renderer } from '../../../sk-core/complets/renderer.js';

import { ATTR_PLUGINS_AN, TPL_PATH_AN, IMPL_PATH_AN } from '../../../sk-core/src/sk-element.js';

import { GridyElement } from "../gridy-element.js";
import { FilterChangeEvent } from "./event/filter-change-event.js";
import { DATA_LOAD_EVT } from "../datasource/event/data-load-event.js";

import { ResLoader } from "../../../sk-core/complets/res-loader.js";

import { DIMPORTS_AN } from "../../../sk-core/src/sk-config.js";

export const FIELD_LABEL_AN = "field-label";
export const FIELD_TITLE_AN = "field-title";
export const FILTER_MODE_AN = "filter-mode";
export const FILTER_INCLUSIVE_AN = "filter-inclusive";	


export class GridyFilter extends GridyElement {


    //renderer: Renderer;
    //theme: string;

    //tplPath;
    //tpl;

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl; // :TODO do as async
    }

    get implClassName() {
        return `GridyFilter${this.capitalizeFirstLetter(this.theme)}`;
    }

    get implPath() {
        return this.getAttribute(IMPL_PATH_AN) ||
            `/node_modules/gridy-grid-${this.theme}/src/gridy-filter-${this.theme}.js`;
    }

    get tplPath() {
        return this.getAttribute(TPL_PATH_AN) || this.impl.tplPath;
    }

    set tplPath(tplPath) {
        this.setAttribute(TPL_PATH_AN, tplPath);
    }

    get input() {
        if (! this._input) {
            this._input = this.querySelector('input');
        }
        return this._input;
    }

    get value() {
        if (this.input) {
            return this.input.value;
        }
        return null;
    }

    changeFilter(event) {
        this.logger.debug('GridyFilter.changeFilter()', arguments);
        let filterAttrs = { value: event.target.value };
        if (this.hasAttribute(FIELD_TITLE_AN)) {
            filterAttrs.field = this.getAttribute(FIELD_TITLE_AN);
        }
        if (this.hasAttribute(FILTER_MODE_AN)) {
            filterAttrs.mode = this.getAttribute(FILTER_MODE_AN);
        }
        this.dispatchEvent(new FilterChangeEvent({
            detail: filterAttrs
        }));
    }
    
    get mode() {
		return this.hasAttribute(FILTER_MODE_AN) ? this.getAttribute(FILTER_MODE_AN) : undefined;
	}

    get configSl() {
        return this.getAttribute('configSl') || 'sk-config, gridy-config';
    }

    get configEl() {
        if (! this._configEl) {
            this._configEl = document.querySelector(this.configSl);
        }
        return this._configEl;
    }
    get driClassName() {
        let driCode = this.hasAttribute('dri')
            ? this.capitalizeFirstLetter(this.css2CamelCase(this.getAttribute('dri'))) : 'Text';
        return `GridyFilter${driCode}`;
    }

    get driPath() {
        let suffix = this.hasAttribute('dri')
            ? this.getAttribute('dri').replace(/-/g, '')
            : null;
        return suffix ? `/node_modules/gridy-filter-${suffix}/src/gridy-filter-${suffix}.js` 
            : `/node_modules/gridy-grid/src/filter/gridy-filter-driver.js`;
    }
    
    async dri() {
        if (! this._dri) {
            let forceInstance = this.hasAttribute('share-dri') ? false : true;
            let dynImportsOff = (this.confValOrDefault(DIMPORTS_AN, null) === "false");
            this._dri = ResLoader.dynLoad(this.driClassName, this.driPath, function(def) {
                return new def(this);
            }.bind(this), forceInstance, false, false ,false, false, false, dynImportsOff);
        } 
        return this._dri; 
        
    }
    
    get fieldTitleLabel() {
		if (this.hasAttribute(FIELD_LABEL_AN)) {
			return this.getAttribute(FIELD_LABEL_AN);
		} else {
			if (this.hasAttribute(FIELD_TITLE_AN) && this.dataSource) {
				let fieldTitle = this.getAttribute(FIELD_TITLE_AN);
				let field = this.dataSource.fieldByTitle(fieldTitle, this.dataSource.fields);
				return field.displayTitle || field.title;
			}
		}
		return '';
    }

    bootstrap() {
        super.bootstrap();
        this.logger.debug('GridyFilter.bootstrap()', arguments);
        if (this.dataSource) {
            this.dataSource.addEventListener(DATA_LOAD_EVT, (event) => {
            });
        }
        if (this.hasAttribute('dri')) {
            this.dri().then((dri) => {
                dri.renderFilter();
            });
        } else {
            if (this.basePath) {
                let context = this;
                this.renderer.mountTemplate(this.tplPath, 'filterTpl', this,
                    { ...context, ...this.tplVars, 'fieldTitle': this.fieldTitleLabel }).then((templateEl) => {
                    this.tpl = templateEl;
                    this.appendChild(this.renderer.prepareTemplate(this.tpl));
                    if (this.dataSource) {
                        let input = this.querySelector('input');
                        if (input != null) {
                            input.addEventListener('input', this.changeFilter.bind(this));
                        }
                    }
                    this.finishRender();
                });
            }
        }
    }

    connectedCallback() {
        this.logger.debug('GridyFilter.connectedCallback()', arguments);
        this.useConfigEl();
        Renderer.configureForElement(this);
        let attrPlugins = this.confValOrDefault(ATTR_PLUGINS_AN, false);
        if (attrPlugins) {
            this.bindAttrPlugins();
        }
        if (this.impl) { // if impl was cached render it
            this.impl.renderImpl();
        }
    }
}

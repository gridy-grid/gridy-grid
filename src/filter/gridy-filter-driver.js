





export class GridyFilterDriver {
    
    constructor(comp) {
		this.comp = comp;
	}
    
    render() {
        return new Promise((resolve, reject) => {
            let context = this;
            this.comp.renderer.mountTemplate(this.tplPath, this.constructor.name + 'Tpl', this.comp, { ...context, ...this.comp.tplVars }).then((templateEl) => {
                this.tpl = templateEl;
                this.comp.appendChild(this.comp.renderer.prepareTemplate(this.tpl));
                //this.finishRender();
                resolve();
            });
        });
    }
}

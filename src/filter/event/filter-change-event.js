

export const FILTER_CHANGE_EVT = 'filterchange';

export class FilterChangeEvent extends CustomEvent {
    constructor(options) {
        super(FILTER_CHANGE_EVT, options);
    }
}

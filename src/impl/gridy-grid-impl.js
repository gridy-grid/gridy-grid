

import { SkComponentImpl } from "../../../sk-core/src/impl/sk-component-impl.js";
import { SK_CONFIG_EL_INDIVIDUAL, TPL_PATH_AN } from "../../../sk-core/src/sk-element.js";
import { SkRenderEvent } from "../../../sk-core/src/event/sk-render-event.js";

export class GridyGridImpl extends SkComponentImpl {

    get container() {
        if (! this._container) {
            this._container = this.comp.el.querySelector('.gridy-container');
        }
        return this._container;
    }

    get tplPath() {
        if (! this._tplPath) {
            this._tplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'gridy-grid.tpl.html';
        }
        return this._tplPath;
    }

    beforeRendered() {
        this.logger.debug('GridyGridImpl.beforeRendered()', arguments);
        super.beforeRendered();
        this.saveState();
    }

    restoreState(state) {
        this.logger.debug('GridyGridImpl.restoreState()', arguments);
        this.container.insertAdjacentHTML('beforeend', state.contentsState);
    }

    afterRendered() {
        this.logger.debug('GridyGridImpl.afterRendered()', arguments);
        super.afterRendered();
        this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
    }

    doRender() {
        this.logger.debug('GridyGridImpl.doRender()', arguments);
        let id = this.getOrGenId();
        this.beforeRendered();
        this.renderWithVars(id);
        this.indexMountedStyles();
        this.afterRendered();
        this.bindEvents();
        this.comp.bindAutoRender();
        this.comp.setupConnections();
        if (this.comp.configEl && this.comp.skConfigElPolicy() === SK_CONFIG_EL_INDIVIDUAL) {
            this.comp.el.appendChild(this.comp.configEl);
        }
        // child elements boostrap
        this.comp.bootstrap();
        // needs to have better after rendered abilities
        this.comp.callPluginHook('onRenderEnd');
        this.comp.implRenderTimest = Date.now();
        this.comp.removeFromRendering();
        this.comp.dispatchEvent(new CustomEvent('rendered', { bubbles: true, composed: true })); // :DEPRECATED
        this.comp.dispatchEvent(new SkRenderEvent({ bubbles: true, composed: true }));
        if (this.comp.renderDeferred) {
            this.comp.renderDeferred.resolve(this.comp);
        }
    }

}


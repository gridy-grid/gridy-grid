
import { GridyConfig } from './gridy-config.js';
import { SkElement } from "../../sk-core/src/sk-element.js";

import { EL_SHARED_ATTRS } from "../../sk-core/src/sk-config.js";

import { SK_RENDER_EVT, SkRenderEvent } from "../../sk-core/src/event/sk-render-event.js";

export const PER_PAGE_AN = "per-page";
export const PER_PAGE_DEFAULT = 10;
export const CUR_PAGE_NUM_AN = "cur-page-num";
export const DS_REF_AN = "ds-ref";
export const PG_REF_AN = "pg-ref";

export const SELECTABLE_AN = "selectable";
export const SELECTABLE_MULTI_AV = "multi";

import { ItemSelectEvent } from "./event/item-select-event.js";
import { ItemDeSelectEvent } from "./event/item-de-select-event.js";

export const ITEM_CSS_CN = "gridy-item";

export const SELECTION_EMIT_AN = "selection-emit";
export const SELECTION_EMIT_TAP_AV = "tap";
export const SELECTION_EMIT_CLICK = "click";
export const SELECTION_EMIT_CTRL_CLICK = "ctrl+click";
export const SELECTION_EMIT_SHIFT_CLICK = "shift+click";
export const SELECTION_EMIT_ALT_CLICK = "alt+click";
export const TIME_TO_SELECT_AN = "time-to-select";
export const TIME_TO_SELECT_DEFAULT = 700;

export const SORT_FIELD_AN = "sort-field";
export const SORT_DIRECTION_AN = "sort-direction";

export const SORT_FIELD_ID_AN = "sort-field-id";
export const SORT_FIELD_ID_DEFAULT = "title";

export class GridyElement extends SkElement {

    get basePath() {
        return this.getAttribute('base-path') || this.getAttribute('basePath')
            || this.getAttribute('basepath') || './src';
    }

    set basePath(basePath) {
        this.setAttribute('base-path', basePath);
    }

    get theme() {
        return this.getAttribute('theme') || 'default';
    }

    set theme(theme) {
        this.setAttribute('theme', theme);
    }

    get lang() {
        return this.getAttribute('lang') || null;
    }

    set lang(lang) {
        this._lang = lang;
    }

    get configTn() {
        return this.getAttribute('config-tn') || 'gridy-config';
    }

    set configTn(configTn) {
        return this.setAttribute('config-tn', configTn);
    }
    
    get sortField() {
        return this.getAttribute(SORT_FIELD_AN);
    }
    
    set sortField(sortField) {
        this.setAttribute(SORT_FIELD_AN, sortField);
    }
    
    get sortDirection() {
        return this.hasAttribute(SORT_DIRECTION_AN) ? this.getAttribute(SORT_DIRECTION_AN) : 'asc';
    }
    
    set sortDirection(sortDirection) {
        this.setAttribute(SORT_DIRECTION_AN, sortDirection);
    }
    
    
    get selectedItems() {
        if (! this._selectedItems) {
            this._selectedItems = new Set();
        }
        return this._selectedItems;
    }
    
    set selectedItems(items) {
        this._selectedItems = items;
    }

    useConfigEl() {
        if (! customElements.get(this.configTn)) {
            customElements.define(this.configTn, GridyConfig);
        }
        if (this.configEl) {
            if (this.configEl.configureElement) {
                this.configEl.configureElement(this);
            } else {
                for (let attrName of Object.keys(EL_SHARED_ATTRS)) {
                    let value = this.getAttribute(attrName);
                    if (value === null && this.configEl.getAttribute(attrName) !== null) {
                        this.setAttribute(attrName, this.configEl.getAttribute(attrName));
                    }
                }
            }
        }
    }

    get configSl() {
        return this.getAttribute('config-sl') || this.getAttribute('configSl') || 'sk-config, gridy-config';
    }

    set configSl(configSl) {
        this.setAttribute('config-sl', configSl);
    }

    get configEl() {
        if (! this._configEl) {
            this._configEl = document.querySelector(this.configSl);
        }
        return this._configEl;
    }

    set configEl(configEl) {
        this._configEl = configEl;
    }
    
    finishRender() {
        this.renderTimest = Date.now();
        this.dispatchEvent(new SkRenderEvent({ bubbles: true, composed: true }));
    }
    
    curPageData(data, backPaged) {
        let pageData = [];
        if (this.pager) {
            let start = 0;
            if (this.pager.curPageNum > 1) {
                start = (this.pager.curPageNum - 1) * this.pager.perPage;
            }
            let end = start + this.pager.perPage;
            if (this.pager.perPage > this.totalResults) {
                end = data.length;
            } else {
                if (this.pager.curPageNum === this.pager.totalPages) {
                    end = (start + ((this.pager.perPage * this.pager.totalPages) - this.pager.totalResults));
                }
            }
            if (! backPaged) {
                for (let i = start; i < end; i++) {
                    if (data[i] !== undefined) {
                        pageData.push(data[i]);
                    }
                }
            } else {
                for (let dataItem of data) {
                    pageData.push(dataItem);
                }
            }
        } else {
            if (this.hasAttribute(PER_PAGE_AN)) {
                let perPage = parseInt(this.getAttribute(PER_PAGE_AN));
                //let curPageNum = parseInt(this.getAttribute(CUR_PAGE_NUM_AN)) || 1;
                let start = (this.curPageNum - 1) * perPage;
                let end = start + perPage;
                for (let i = start; i < end; i++) {
                    if (data[i] !== undefined) {
                        pageData.push(data[i]);
                    }
                }
            } else { // no pager no per-page attr
                if (this.perPageEl) { // but has per page selection widget
                    let perPage = this.perPageEl.perPage;
                    let start = (this.curPageNum - 1) * perPage;
                    let end = start + perPage;
                    for (let i = start; i < end; i++) {
                        if (data[i] !== undefined) {
                            pageData.push(data[i]);
                        }
                    }
                } else { // absolutely no paging
                    for (let dataItem of data) {
                        pageData.push(dataItem);
                    }
                }
            }
        }
        return pageData;
    }
    
    guessFieldId(sortField) {
        let field = this.dataSource.findField(sortField);
        let sortFieldId = this.confValOrDefault(SORT_FIELD_ID_AN, SORT_FIELD_ID_DEFAULT);
        if (field && field[sortFieldId]) {
            return field[sortFieldId];
        }
        return field ? field.name ? field.name : field.title ? field.title: field.path : null;
    }
    
    onPageChanged(event) {
        if (this.backPaged) {
            this.dataSource.clearData();
            this.renderData();
            let params = {};
            if (this.pager) {
                params.page = this.pager.curPageNum || 1;
            }
            if (this.sortField) {
                params.sortField = this.guessFieldId(this.sortField);
                params.sortDirection = this.sortDirection;
            }
            this.dataSource.fetchData(Object.assign({
                backPaged: true
            }, params)).then((detail) => {
                this.dataSource.loadData(null, detail);
            });
        } else {
            this.renderData(event);
        }
    }
    
    dsLoadInitial() {
        if (this.dataSource.backPaged) {
            this.dataSource.clearData();
            let params = {};
            if (this.pager) {
                params.page = this.pager.curPageNum || 1;
            }
            if (this.sortField) {
                params.sortField = this.guessFieldId(this.sortField);
                params.sortDirection = this.sortDirection;
            }
            this.dataSource.fetchData(Object.assign({
                backPaged: true,
            }, params)).then((detail) => {
                this.dataSource.loadData(null, detail);
            });
        }
    }
    
    rowsEqual(rowA, rowB) {
        return (this.dataSource && typeof this.dataSource.itemsEqual === 'function')
            ? this.dataSource.itemsEqual(rowA.dataItem, rowB.dataItem) : rowA.dataItem === rowB.dataItem;
    }
    
    get isMultiSelect() {
        return this.getAttribute(SELECTABLE_AN) === SELECTABLE_MULTI_AV;
    }
    
    get timeToSelect() {
        return this.confValOrDefault(TIME_TO_SELECT_AN, TIME_TO_SELECT_DEFAULT);
    }
    
    get selectionEmit() {
        return this.confValOrDefault(SELECTION_EMIT_AN, false);
    }
    
    findItemEl(el) {
        if (el.classList.contains(ITEM_CSS_CN) || el.tagName.toUpperCase() === 'TR') {
            return el;
        } else {
            if (el.parentElement) {
                return this.findItemEl(el.parentElement);
            } else {
                return null;
            }
        }
    }
    
    checkAndToggleSelection(event, realTarget) {
        let itemEl = realTarget ? realTarget : this.findItemEl(event.target);
        if (itemEl) {
            if (! this.selectionEmit // click 
                || (this.selectionEmit === SELECTION_EMIT_CLICK)
                || (this.selectionEmit === SELECTION_EMIT_CTRL_CLICK && event.ctrlKey)
                || (this.selectionEmit === SELECTION_EMIT_SHIFT_CLICK && event.shiftKey)
                || (this.selectionEmit === SELECTION_EMIT_ALT_CLICK && event.altKey)) {
            this.toggleRowSelection(itemEl);
            }
        }
    }
    
    bindSelectable(target) {
        if (! target) {
            target = this;
        }
        if (this.selectionEmit === SELECTION_EMIT_TAP_AV) {
            target.addEventListener('mousedown', function(event) {
                this.selectDelay = setTimeout(this.checkAndToggleSelection.bind(this, event, target), this.timeToSelect);
            }.bind(this), true);
            target.addEventListener('mouseup', function(event) {
                clearTimeout(this.selectDelay);
            }.bind(this));
        } else { // click
            target.addEventListener('click', this.checkAndToggleSelection.bind(this));
        }

    }
    
    toggleRowSelection(item) {
        let itemsSelected = false;
        if (this.isMultiSelect) {
            if (this.selectedItems.has(item)) {
                this.selectedItems.delete(item);
            } else {
                this.selectedItems.add(item);
                this.selectedItem = item;
                this.prevSelectedItem = this.selectedItem;
                itemsSelected = true;
            }
        } else {
            this.selectedItems = new Set();
            if (this.selectedItem && this.rowsEqual(this.selectedItem, item)) {
                this.selectedItem = null;
            } else {
                this.prevSelectedItem = this.selectedItem;
                this.selectedItem = item;
                itemsSelected = true;
                this.selectedItems.add(item);
            }
        }
        if (typeof this.impl.toggleItemSelection === 'function') {
            if ((! this.isMultiSelect) && this.prevSelectedItem) { // deselect old row
                this.impl.toggleItemSelection(this.prevSelectedItem);
            }
            this.impl.toggleItemSelection(item);
        }
        if (itemsSelected) {
            this.dispatchEvent(new ItemSelectEvent({ detail: { item: item }, bubbles: true, composed: true}));
        } else {
            this.dispatchEvent(new ItemDeSelectEvent({ detail: { item: item }, bubbles: true, composed: true}));
        }
    }
    
    bootstrap() {
        this.loadStoredCfg();
    }
}

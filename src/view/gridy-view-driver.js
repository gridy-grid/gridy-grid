import { ResLoader } from "../../../sk-core/complets/res-loader.js";
import { JsonPath, JSONPATH_CN, JSONPATH_PT } from "../../../sk-core/src/json-path.js";

export const ITEM_TPL_STR_AN = "item-tpl-str";

export const ITEM_TPL_FULLPATH_AN = "item-tpl-fullpath";

export const VIEW_CONTAINER_SL = ".gridy-view-container";


export class GridyViewDriver {
	
	constructor(comp) {
		this.comp = comp;
	}
    
    get reqFields() {
		if (! this._reqFields) {
			this._reqFields = [ 'name', 'url' ];
		}
		return this._reqFields;
	}
	
	set reqFields(fields) {
		this._reqFields = fields;
	}
    
    get idxFields() {
		if (! this._idxFields) {
			this._idxFields = [];
		}
		return this._idxFields;
	}
	
	set idxFields(idxFields) {
		this._idxFields = idxFields;
	}
    
    get byField() {
		if (! this._byField) {
			this._byField = {};
		}
		return this._byField;
	}

    get jsonPath() {
        if (! this._jsonPath) {
            this._jsonPath = this.comp && this.comp.jsonPath ? this.comp.jsonPath :
	            ResLoader.dynLoad(JSONPATH_CN, JSONPATH_PT, function(def) {
		            return new def();
	            }.bind(this), false, false, false ,false, false, false, true, true, JsonPath);
        }
        return this._jsonPath;
    }

    set jsonPath(jsonPath) {
        this._jsonPath = jsonPath;
    }

    attrOrDefault(attrName, deflt) {
        return this.comp.hasAttribute(attrName) ? this.comp.getAttribute(attrName) : deflt;
    }
	
    configFromEl(el) {
        this.mapFieldsFromAttrs(el);
    }

    mapFieldsFromAttrs(el) {
        this.fieldMappings = {};
        for (let attr of el.attributes) {
            let res = attr.name.match(/field-(.*)/);
            if (res) {
                let fieldKey = res[1];
                let fieldTitle = el.getAttribute(attr.name);
                let field = this.fieldByTitle(fieldTitle, el.dataSource.fields);
                if (field) {
                    this.fieldMappings[fieldKey] = field;
                }
            }
        }
    }

    fieldByTitle(title, fields) {
        for (let field of fields) {
            if (field.title === title) {
                return field;
            }
        }
        return null;
    }
    
    indexRow(valItem) {
        for (let reqField of this.idxFields) {
            if (!this.byField[reqField]) {
                this.byField[reqField] = new Map();
            }
            this.byField[reqField].set(valItem[reqField], valItem);
        }
    }

    async fmtData(data) {
        let dsData = [];
        for (let dataItem of data) {
            let valItem = {};
            for (let fieldKey of Object.keys(this.fieldMappings)) {
                let field = this.fieldMappings[fieldKey];
                let onValue = this.jsonPath.query(dataItem, field.path);
                let value = onValue[0];
                if (value === undefined) {
                    value = dataItem;
                }
                valItem[fieldKey] = value;
            }

            if (this.validateRow(valItem)) {
	            dsData.push(valItem);
	        }
            this.indexRow(valItem);
		}
        return dsData;
    }
    
    validateRow(row) {
		for (let fieldKey of Object.keys(this.fieldMappings)) {
			let field = this.fieldMappings[fieldKey];
			if ((field.required || this.reqFields.indexOf(fieldKey) > -1) && ! row[fieldKey]) {
				return false;
			}
		}
		return true;
	}
    
    get itemTplFullPath() {
        if (! this._itemTplFullPath) {
            this._itemTplFullPath = this.comp.hasAttribute(ITEM_TPL_FULLPATH_AN)
                ? this.getAttribute(ITEM_TPL_FULLPATH_AN) : null;
        }
        return this._itemTplFullPath;
    }
    
    renderItems(target, fmtedData, tpl) {
        for (let item of fmtedData) {
            let html = '';
            if (tpl) {
                if (typeof tpl === 'string') {
                    let itemContentsEl = this.comp.renderer.createEl('div');
                    itemContentsEl.insertAdjacentHTML('beforeend', tpl);
                    html = this.comp.renderer.renderMustacheVars(itemContentsEl, item);
                } else {
                    let prepared = this.comp.renderer.prepareTemplate(tpl);
                    html = this.comp.renderer.renderMustacheVars(prepared, {...this.comp.tplVars, ...item});
                }
            } else {
                for (let fieldKey of Object.keys(this.fieldMappings)) {
                    html += `<span>${item[fieldKey]}</span>`;
                }
            }
            target.insertAdjacentHTML('beforeend', html);
            let items = target.querySelectorAll('.gridy-item');
            let curItem = items[items.length - 1];
            curItem.dataItem = item;
        }
    }
    
    async renderData(el, data) {
        return new Promise(async (resolve, reject) => {
            this.type = el.getAttribute('type');
            this.mapFieldsFromAttrs(el);
            let fmtedData = await this.fmtData(data);
            this.container = el.querySelector(VIEW_CONTAINER_SL);
            this.container.innerHTML = '';
            if (this.comp.hasAttribute(ITEM_TPL_STR_AN)) {
                let itemTplStr = this.comp.getAttribute(ITEM_TPL_STR_AN);
                this.renderItems(this.container, fmtedData, itemTplStr);
                resolve(this);
            } else {
                if (this.itemTplFullPath) {
                    if (! this.comp.itemTpl) {
                        const loadTpl = async () => {
                            this.comp.itemTpl = await this.comp.renderer.mountTemplate(this.itemTplFullPath, this.constructor.name + 'ItemTpl',
                                this.comp, {
                                    themePath: this.comp.impl.themePath, ...this.comp.tplVars});
                        };
                        loadTpl().then((tpl) => {
                            this.renderItems(this.container, fmtedData, this.comp.itemTpl);
                            resolve(this);
                        });
                    } else {
                        this.renderItems(this.container, fmtedData, this.comp.itemTpl);
                        resolve(this);
                    }
                } else {
                    this.renderItems(this.container, fmtedData);
                    resolve(this);
                }
            }
        });
	}    
}

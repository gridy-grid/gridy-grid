
import { GridyViewDriver } from "../gridy-view-driver.js";

export class GridyViewList extends GridyViewDriver {
    
    async renderData(el, data) {

		this.type = el.getAttribute('type');
        this.mapFieldsFromAttrs(el);
		let fmtedData = await this.fmtData(data);
        let container = el.querySelector('.gridy-view-container');
        container.innerHTML = '';
        for (let item of fmtedData) {
            let html = '<div>';
                for (let fieldKey of Object.keys(this.fieldMappings)) {
                    html +=`<span>${item[fieldKey]}</span>`;
                }
            html += '</div>'
            container.insertAdjacentHTML('beforeend', html);
        }
        return Promise.resolve(this);
	}
}

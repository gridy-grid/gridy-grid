
import { TPL_PATH_AN } from "../../../../sk-core/src/sk-element.js";

import { SkComponentImpl } from "../../../../sk-core/src/impl/sk-component-impl.js";

export class GridyViewImpl extends SkComponentImpl {

    get tplPath() {
        if (! this._tplPath) {
            this._tplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'view.tpl.html';
        }
        return this._tplPath;
    }

    renderImpl() {
        super.renderImpl();
        this.comp.bootstrap();
        if (this.comp.renderDeferred) {
            this.comp.renderDeferred.resolve(this.comp);
        }
    }
    
        
    get selectedCssClass() {
        return 'selected';
    }
    
    toggleItemSelection(item) {
        if (item) {
            if (! item.classList.contains(this.selectedCssClass)) {
                item.classList.add(this.selectedCssClass);
            } else {
                item.classList.remove(this.selectedCssClass);
            }
        }
    }
}


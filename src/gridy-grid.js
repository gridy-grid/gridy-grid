
import { Renderer } from "../../sk-core/complets/renderer.js";

import { GridyFilter } from "./filter/gridy-filter.js";
import { GridyPager } from "./pager/gridy-pager.js";
import { GridyTable } from "./table/gridy-table.js";
import { GridyTableInfo } from "./table/gridy-table-info.js";
import { GridySpinner } from "./gridy-spinner.js";
import { DataSourceLocal } from "./datasource/data-source-local.js";
import { DataSourceAjax } from "./datasource/data-source-ajax.js";
import { GridyDataSource } from "./datasource/gridy-data-source.js";
import { GridyDataField } from "./datasource/gridy-data-field.js";
import { GridyConfig } from "./gridy-config.js";
import {ATTR_PLUGINS_AN, IMPL_PATH_AN} from "../../sk-core/src/sk-element.js";
import { GridyElement, CUR_PAGE_NUM_AN, PER_PAGE_AN, SORT_DIRECTION_AN, SORT_FIELD_AN } from "./gridy-element.js";
import { GridyPerPage } from "./perpage/gridy-per-page.js";
import { GridyChart } from "./chart/gridy-chart.js";
import { GridyView } from "./view/gridy-view.js";
import { FILTER_CHANGE_EVT, FilterChangeEvent } from "./filter/event/filter-change-event.js";
import { PerPageChangeEvent } from "./perpage/event/per-page-change-event.js";
import { PageChangeEvent, PAGE_CHANGE_EVT } from "./pager/event/page-change-event.js";
import { DATA_LOAD_EVT } from "./datasource/event/data-load-event.js";
import { DATA_FETCH_START_EVT } from "./datasource/event/data-fetch-start-event.js";
import { DATA_LOAD_START_EVT } from "./datasource/event/data-load-start-event.js";
import { BOOTSTRAP_EVT, BootstrapEvent } from "./event/boostrap-event.js";
import { DATA_CLEAR_EVT } from "./datasource/event/data-clear-event.js";
import { GridyHeaderMenu } from "./table/gridy-header-menu.js";
import { TABLE_HIDE_COLUMNS_EVT } from "./table/event/table-hide-columns-event.js";
import { GridyToolbar } from "./gridy-toolbar.js";

export class GridyGrid extends GridyElement {

    // filter: GridyFilter;
    // pager: GridyPager;
    // table: GridyTable;
    // info: GridyTableInfo;

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl; // :TODO do as async
    }

    get implClassName() {
        return `GridyGrid${this.capitalizeFirstLetter(this.theme)}`;
    }

    get implPath() {
        return this.getAttribute(IMPL_PATH_AN) ||
            `/node_modules/gridy-grid-${this.theme}/src/gridy-grid-${this.theme}.js`;
    }

    set dataSource(dataSource) {
        this._dataSource = dataSource;
        this._dataSource.logger = this.logger;
    }

    get dataSource() {
        return this._dataSource;
    }

    get sortField() {
        return this.getAttribute(SORT_FIELD_AN);
    }

    set sortField(sortField) {
        return this.setAttribute(SORT_FIELD_AN, sortField);
    }

    get sortDirection() {
        return this.getAttribute(SORT_DIRECTION_AN);
    }

    set sortDirection(sortDirection) {
        return this.setAttribute(SORT_DIRECTION_AN, sortDirection);
    }

    get gridTn() {
        return this.getAttribute('grid-tn') || this.tagName.toLowerCase();
    }

    set gridTn(gridTn) {
        return this.setAttribute('grid-tn', gridTn);
    }

    get tableTn() {
        return this.getAttribute('table-tn') || 'gridy-table';
    }

    set tableTn(tableTn) {
        return this.setAttribute('table-tn', tableTn);
    }

    get dsTn() {
        return this.getAttribute('ds-tn') || 'gridy-data-source';
    }

    set dsTn(dsTn) {
        return this.setAttribute('ds-tn', dsTn);
    }

    get dfTn() {
        return this.getAttribute('df-tn') || 'gridy-data-field';
    }

    set dfTn(dfTn) {
        return this.setAttribute('df-tn', dfTn);
    }

    get filterTn() {
        return this.getAttribute('filter-tn') || 'gridy-filter';
    }

    set filterTn(filterTn) {
        return this.setAttribute('filter-tn', filterTn);
    }

    get pagerTn() {
        return this.getAttribute('pager-tn') || 'gridy-pager';
    }

    set pagerTn(pagerTn) {
        return this.setAttribute('gridy-pager', pagerTn);
    }

    get infoTn() {
        return this.getAttribute('info-tn') || 'gridy-table-info';
    }

    set infoTn(infoTn) {
        return this.setAttribute('info-tn', infoTn);
    }

    get chartTn() {
        return this.getAttribute('chart-tn') || 'gridy-chart';
    }

    set chartTn(chartTn) {
        return this.setAttribute('chart-tn', chartTn);
    }

    get viewTn() {
        return this.getAttribute('view-tn') || 'gridy-view';
    }

    set viewTn(viewTn) {
        return this.setAttribute('view-tn', viewTn);
    }
    
    get spinnerTn() {
        return this.getAttribute('spinner-tn') || 'gridy-spinner';
    }

    set spinnerTn(spinnerTn) {
        return this.setAttribute('spinner-tn', spinnerTn);
    }

    get perPageTn() {
        return this.getAttribute('per-page-tn') || 'gridy-per-page';
    }

    set perPageTn(perPageTn) {
        return this.setAttribute('per-page-tn', perPageTn);
    }
    
    get headerMenuTn() {
        return this.getAttribute('header-menu-tn') || 'gridy-header-menu';
    }
    
    set headerMenuTn(headerMenuTn) {
        return this.setAttribute('header-menu-tn', headerMenuTn);
    }
    
    get toolbarTn() {
        return this.getAttribute('toolbar-tn') || 'gridy-toolbar';
    }
    
    set toolbarTn(toolbarTn) {
        return this.setAttribute('toolbar-tn', toolbarTn);
    }
    
    get dialogTn() {
        return this.getAttribute('dialog-tn') || 'sk-dialog';
    }
    
    set dialogTn(dialogTn) {
        return this.setAttribute('dialog-tn', dialogTn);
    }
    
    forwardProp(propName, target) {
        this.logger.debug('GridyGrid.forwardProp()', arguments);
        if (this[propName]) {
            target[propName] = this[propName];
        }
    }

    forwardProps(props, target) {
        this.logger.debug('GridyGrid.forwardProps()', arguments);
        for (let prop of props) {
            this.forwardProp(prop, target);
        }
    }
    
    get curPageNum() {
        let pageNum = parseInt(this.getAttribute(CUR_PAGE_NUM_AN)) || 1;
        if (this.pager) {
            pageNum = this.pager.curPageNum;
        }
        return pageNum;
    }
    
    set curPageNum(pageNum) {
        this.setAttribute(CUR_PAGE_NUM_AN, pageNum);
    }
    
    get supportedElements() {
        this.logger.debug('GridyGrid.supportedElements');
        let elements = {};
        elements[this.dsTn] = GridyDataSource;
        elements[this.dfTn] = GridyDataField;
        elements[this.configTn] = GridyConfig;
        elements[this.tableTn] = GridyTable;
        elements[this.filterTn] = GridyFilter;
        elements[this.pagerTn] = GridyPager;
        elements[this.infoTn] = GridyTableInfo;
        elements[this.chartTn] = GridyChart;
        elements[this.viewTn] = GridyView;
        elements[this.spinnerTn] = GridySpinner;
        elements[this.perPageTn] = GridyPerPage;
        elements[this.headerMenuTn] = GridyHeaderMenu;
        elements[this.toolbarTn] = GridyToolbar;
        return elements;
    }

    regCustomEl(el) {
        this.logger.debug('GridyGrid.regCustomEl()', arguments);
        this.regCustomElOrRender(el);
    }

    regCustomEls(els) {
        this.logger.debug('GridyGrid.regCustomEls()', arguments);
        for (let el of els) {
            this.regCustomEl(el);
        }
    }

    regCustomElOrRender(name, el) {
        this.logger.debug('GridyGrid.regCustomElOrRender()', arguments);
        if (!customElements.get(name)) {
            customElements.define(name, this.supportedElements[name]);
        } else {
            if (el) {
                el.connectedCallback();
            }
        }
    }
    
    bootstrapSpinner() {
        let spinner = this.el.querySelector(this.spinnerTn);
        if (spinner !== null) {
            this.forwardProps(['renderer', 'basePath', 'theme'], spinner);
            this.spinner = spinner;
            this.regCustomElOrRender(this.spinnerTn, this.spinner);
            if (this.dataSource) {
                if (this.dataSource instanceof DataSourceAjax) {
                    this.dataSource.addEventListener(DATA_FETCH_START_EVT, (event) => {
                        spinner.show();
                    });
                } else {
                    this.dataSource.addEventListener(DATA_LOAD_START_EVT, (event) => {
                        spinner.show();
                    });
                }
                this.dataSource.addEventListener(DATA_LOAD_EVT, (event) => {
                    spinner.hide();
                });
            }
            this.spinner.whenRendered(() => {
                console.log('spinner rendered');
            });
        }
        return this.spinner;
    }
    
    bootstrapFilters() {
        this.filters = [];
        let filters = this.el.querySelectorAll(this.filterTn);
        for (let filter of filters) {
            this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme'], filter);
            this.filters.push(filter);
        }
        return this.filters;
    }
    
    bootstrapPerPage() {
        let perPageEl = this.el.querySelector(this.perPageTn);
        if (perPageEl !== null) {
            this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme'], perPageEl);
            this.perPageEl = perPageEl;
            this.regCustomElOrRender(this.perPageTn, this.perPageEl);
        }
        return this.perPageEl;
    }

    bootstrapPager() {
        let pager = this.el.querySelector(this.pagerTn);
        if (pager !== null) {
            this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme', 'curPageNum'], pager);
            if (this.dataSource) {
                if (this.dataSource instanceof DataSourceAjax) {
                    this.dataSource.addEventListener(DATA_FETCH_START_EVT, (event) => {
                        pager.disabled = true;
                    });
                    this.dataSource.addEventListener(DATA_LOAD_EVT, (event) => {
                        pager.disabled = false;
                    });
                }
            }
            if (this.hasAttribute(PER_PAGE_AN)) {
                pager.setAttribute(PER_PAGE_AN, this.getAttribute(PER_PAGE_AN));
            }
            if (this.perPageEl) {
                pager.perPageEl = this.perPageEl;
            }
            pager.addEventListener(PAGE_CHANGE_EVT, () => {
                this.withUpdatesLocked(() => {
                    this.setAttribute(CUR_PAGE_NUM_AN, pager.curPageNum);
                });
            })
            this.pager = pager;
            this.regCustomElOrRender(this.pagerTn, this.pager);
        }
        return this.pager;
    }
    
    bootstrapTable() {
        let table = this.el.querySelector(this.tableTn);
        if (table !== null) {
            this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme', 'pager',
                'filters', 'onrowclick', 'onrowrclick', 'onhrowclick', 'gridTn', 'selectable'], table);
            if (this.dataSource) {
                if (this.dataSource instanceof DataSourceAjax) {
                    this.dataSource.addEventListener(DATA_FETCH_START_EVT, (event) => {
                        table.disabled = true;
                    });
                    this.dataSource.addEventListener(DATA_LOAD_EVT, (event) => {
                        table.disabled = false;
                    });
                }
            }
            if (this.hasAttribute(PER_PAGE_AN)) {
                table.setAttribute(PER_PAGE_AN, this.getAttribute(PER_PAGE_AN));
            }
            table.headers = true;
            if (!table.sortField && this.sortField) {
                table.sortField = this.sortField;
                table.setAttribute(SORT_FIELD_AN, this.sortField);
            }
            if (!table.sortDirection && this.sortDirection) {
                table.sortDirection = this.sortDirection;
                table.setAttribute(SORT_DIRECTION_AN, this.sortDirection);
            }
            if (this.perPageEl) {
                table.perPageEl = this.perPageEl;
            }
            this.table = table;
            this.regCustomElOrRender(this.tableTn, this.table);
            let headerMenu = this.el.querySelector(this.headerMenuTn);
            if (headerMenu !== null) {
                this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme', 'pager', 'gridTn'], headerMenu);
                headerMenu.table = this.table;
                this.headerMenu = headerMenu;
                this.table.whenRendered(() => {
                    this.regCustomElOrRender(this.dialogTn);
                    this.table.buildHeaderMenu(this.headerMenu);
                    this.regCustomElOrRender(this.headerMenuTn, this.table);
                    this.table.bindHeaderMenu(this.headerMenu);
                });
                this.headerMenu.addEventListener(TABLE_HIDE_COLUMNS_EVT, this.table.hideColumns.bind(this.table));
            }
        }
        return this.table;
    }
    
    bootstrapInfos() {
        this.infos = [];
        let infos = this.el.querySelectorAll(this.infoTn);
        for (let info of infos) {
            if (info != null) {
                this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme', 'pager', 'lang'], info);
                if (this.dataSource) {
                    this.dataSource.addEventListener(DATA_CLEAR_EVT, function(event) {
                        info.clear();
                    });
                }
                if (this.table) {
                    this.table.addEventListener(PAGE_CHANGE_EVT, function(event) {
                        info.dispatchEvent(new PageChangeEvent({
                            detail: {
                                page: event.detail.page,
                                backPaged: this.dataSource.backPaged
                            }
                        }));
                    }.bind(this));
                    if (this.table.hasAttribute(PER_PAGE_AN)) {
                        info.setAttribute(PER_PAGE_AN, table.getAttribute(PER_PAGE_AN));
                    }
                }
                if (this.perPageEl) {
                    info.perPageEl = this.perPageEl;
                }
                if (this.filters) {
                    for (let filter of this.filters) {
                        filter.addEventListener(FILTER_CHANGE_EVT, (event) => {
                            info.dispatchEvent(new FilterChangeEvent({
                                detail: { value: event.target.value }
                            }));
                        });
                    }
                }
                this.infos.push(info);
                this.regCustomElOrRender(this.infoTn, info);
            }
        }
        return this.infos;
    }
    
    bootstrapCharts() {
        this.charts = [];
        let charts = this.el.querySelectorAll(this.chartTn);
        for (let chart of charts) {
            if (chart != null) {
                this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme', 'pager', 'lang'], chart);
            
                if (this.filters) {
                    for (let filter of this.filters) {
                        filter.addEventListener(FILTER_CHANGE_EVT, (event) => {
                            chart.dispatchEvent(new FilterChangeEvent({
                                detail: { value: event.target.value }
                            }));
                        });
                    }
                }
                if (!chart.sortField && this.sortField) {
                    chart.sortField = this.sortField;
                    chart.setAttribute(SORT_FIELD_AN, this.sortField);
                }
                if (!chart.sortDirection && this.sortDirection) {
                    chart.sortDirection = this.sortDirection;
                    chart.setAttribute(SORT_DIRECTION_AN, this.sortDirection);
                }
                this.charts.push(chart);
                this.regCustomElOrRender(this.chartTn, chart);
            }
        }
        return this.charts;
    }
    
    bootstrapToolbars() {
        this.toolbars = [];
        let toolbars = this.el.querySelectorAll(this.toolbarTn);
        for (let toolbar of toolbars) {
            if (toolbar != null) {
                this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme', 'pager', 'lang'], toolbar);

                this.toolbars.push(toolbar);
                this.regCustomElOrRender(this.toolbarTn, toolbar);
            }
        }
        return this.toolbars;
    }
    
    bootstrapViews() {
        this.views = [];
        let views = this.el.querySelectorAll(this.viewTn);
        for (let view of views) {
            if (view != null) {
                this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme', 'pager', 'lang'], view);
            
                if (this.filters) {
                    for (let filter of this.filters) {
                        filter.addEventListener(FILTER_CHANGE_EVT, (event) => {
                            view.dispatchEvent(new FilterChangeEvent({
                                detail: { value: event.target.value }
                            }));
                        });
                    }
                }
                if (!view.sortField && this.sortField) {
                    view.sortField = this.sortField;
                    view.setAttribute(SORT_FIELD_AN, this.sortField);
                }
                if (!view.sortDirection && this.sortDirection) {
                    view.sortDirection = this.sortDirection;
                    view.setAttribute(SORT_DIRECTION_AN, this.sortDirection);
                }
                this.views.push(view);
                this.regCustomElOrRender(this.viewTn, view);
            }
        }
    }
    
    bootstrapFeatures() {
        this.bootstrapFilters();
        this.bootstrapPerPage();
        this.bootstrapPager();
        this.bootstrapTable();
        this.bootstrapInfos();
        this.bootstrapViews();
        this.bootstrapCharts();
        this.bootstrapToolbars();
        this.bootstrapTimest = Date.now();
    }
    
    loadDataRef() {
        if (this.dataSourceEl
            && this.dataSourceEl.getAttribute('datasource-type') === 'DataSourceLocal') {
            this.dataSourceEl.loadDataRef();
        }
    }
    
    finishBootstrap() {
        this.regCustomEls([this.spinnerTn, this.filterTn, this.pagerTn, this.tableTn, this.infoTn, this.chartTn, this.dfTn, this.perPageTn]);
    
        this.loadDataRef();
        
        this.dispatchEvent(new BootstrapEvent({ bubbles: true, composed: true }));
        this.dispatchEvent(new CustomEvent('bootstraped', { bubbles: true, composed: true })); // :DEPRECATED
    }
    
    bootstrap() {
        super.bootstrap();
        this.logger.debug('GridyGrid.bootstrap()', arguments);

        if (!this.dataSource) {
            let dataSourceEl = this.el.querySelector(this.dsTn);
            if (dataSourceEl != null) {
                this.regCustomEl(this.dsTn);
                //dataSourceEl.config();
                this.dataSourceEl = dataSourceEl;
                this.dataSource = dataSourceEl.dataSource;
            }
        }
        let spinner = this.bootstrapSpinner();
        if (spinner) {
            this.spinner.whenRendered(() => {
                this.bootstrapFeatures();
                this.finishBootstrap();
            });
        } else {
            this.bootstrapFeatures();
            this.finishBootstrap();
        }
    }

    connectedCallback() {
        this.logger.debug('GridyGrid.connectedCallback()', arguments);
        this.setupConfigEl();
        this.useConfigEl();
        this.setupRenderer();
        let attrPlugins = this.confValOrDefault(ATTR_PLUGINS_AN, false);
        if (attrPlugins) {
            this.bindAttrPlugins();
        }
        if (this.useShadowRoot) {
            if (! this.shadowRoot) {
                this.attachShadow({mode: 'open'});
            }
        }
        if (this.impl) { // if impl was cached render it
            this.impl.renderImpl();
        }
    }

    changePage(pageNum) {
        let totalPages = this.pager ? this.pager.totalPages : this.dataSource.totalPages;
        if (pageNum < 0 || pageNum > totalPages) {
            this.logger.warn(`changePage called to out of range, ${pageNum}`);
            return false;
        }
        if (this.pager) {
            this.pager.setAttribute(CUR_PAGE_NUM_AN, pageNum);
        } else {
            if (this.table) {
                this.table.dispatchEvent(new PageChangeEvent({
                    detail: {
                        page: pageNum,
                        backPaged: this.dataSource.backPaged
                    }
                }));
            }
            if (this.infos) {
                for (let info of this.infos) {
                    info.dispatchEvent(new PageChangeEvent({
                        detail: {
                            page: pageNum,
                            backPaged: this.dataSource.backPaged
                        }
                    }));
                }
            }
        }
        this.curPageNum = pageNum;
        this.setAttribute(CUR_PAGE_NUM_AN, pageNum);
    }

    changePerPage(perPage) {
        if (this.perPageEl) {
            this.perPageEl.perPage = perPage;
            this.perPageEl.dispatchEvent(new PerPageChangeEvent({ detail: { perPage: perPage }}));
        } else {
            if (this.pager) {
                this.pager.dispatchEvent(new PerPageChangeEvent({ detail: { perPage: parseInt(perPage) } }));
            }
            if (this.table) {
                this.table.setAttribute('per-page', perPage);
                this.table.dispatchEvent(new PerPageChangeEvent({
                    detail: {
                        perPage: parseInt(perPage),
                    }
                }));
            }
            if (this.infos) {
                for (let info of this.infos) {
                    info.setAttribute('per-page', perPage);
                    info.dispatchEvent(new PerPageChangeEvent({
                        detail: {
                            perPage: parseInt(perPage),
                        }
                    }));
                }
            }
        }
    }

    nextPage() {
        let pageNum = parseInt(this.getAttribute(CUR_PAGE_NUM_AN)) || 1;
        if (this.pager) {
            pageNum = this.pager.curPageNum + 1;
        } else {
            pageNum = pageNum + 1;
        }
        this.changePage(pageNum);
    }

    prevPage() {
        let pageNum = parseInt(this.getAttribute(CUR_PAGE_NUM_AN)) || 1;
        if (this.pager) {
            pageNum = this.pager.curPageNum - 1;
        } else {
            pageNum = pageNum - 1;
        }
        this.changePage(pageNum);
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === CUR_PAGE_NUM_AN && oldValue !== newValue && newValue !== this.curPageNum && ! this.updatesLocked) {
            if (this.bootstrapTimest) {
                this.changePage(newValue);
            }
        }
        if (name === PER_PAGE_AN && oldValue !== newValue && ! this.updatesLocked) {
            if (this.bootstrapTimest) {
                this.changePerPage(newValue);
            }
        }
    }

    static get observedAttributes() {
        return [ CUR_PAGE_NUM_AN, PER_PAGE_AN ];
    }

	whenBootstraped(callback) {
        return Renderer.callOnceOn(this, BOOTSTRAP_EVT, callback);
	}
}

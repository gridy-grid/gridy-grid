
import { JsonPath, JSONPATH_CN, JSONPATH_PT } from "../../../../sk-core/src/json-path.js";
import { ResLoader } from "../../../../sk-core/complets/res-loader.js";


export class FieldMappedChart {
	
	constructor(el) {
		this.el = el;
	}
	
	get reqFields() {
		if (! this._reqFields) {
			this._reqFields = [];
		}
		return this._reqFields;
	}
	
	set reqFields(fields) {
		this._reqFields = fields;
	}
    
    get idxFields() {
		if (! this._idxFields) {
			this._idxFields = [];
		}
		return this._idxFields;
	}
	
	set idxFields(idxFields) {
		this._idxFields = idxFields;
	}
    
    get byField() {
		if (! this._byField) {
			this._byField = {};
		}
		return this._byField;
	}

    get jsonPath() {
        if (! this._jsonPath) {
	        this._jsonPath = this.el && this.el.jsonPath ? this.el.jsonPath :
		        ResLoader.dynLoad(JSONPATH_CN, JSONPATH_PT, function(def) {
			        return new def();
		        }.bind(this), false, false, false ,false, false, false, true, true, JsonPath);
		        
        }
        return this._jsonPath;
    }

    set jsonPath(jsonPath) {
        this._jsonPath = jsonPath;
    }

    attrOrDefault(attrName, deflt) {
        return this.el.hasAttribute(attrName) ? this.el.getAttribute(attrName) : deflt;
    }
	
    configFromEl(el) {
        this.mapFieldsFromAttrs(el);
    }

    mapFieldsFromAttrs(el) {
        this.fieldMappings = {};
        for (let attr of el.attributes) {
            let res = attr.name.match(/field-(.*)/);
            if (res) {
                let fieldKey = res[1];
                let fieldTitle = el.getAttribute(attr.name);
                let field = this.fieldByTitle(fieldTitle, el.dataSource.fields);
                if (field) {
                    this.fieldMappings[fieldKey] = field;
                }
            }
        }
    }

    fieldByTitle(title, fields) {
        for (let field of fields) {
            if (field.title === title) {
                return field;
            }
        }
        return null;
    }
    
    indexRow(valItem) {
        for (let reqField of this.idxFields) {
            if (!this.byField[reqField]) {
                this.byField[reqField] = new Map();
            }
            this.byField[reqField].set(valItem[reqField], valItem);
        }
    }

    async fmtData(data) {
        let dsData = [];
        for (let dataItem of data) {
            let valItem = {};
            for (let fieldKey of Object.keys(this.fieldMappings)) {
                let field = this.fieldMappings[fieldKey];
                let onValue = this.jsonPath.query(dataItem, field.path);
                let value = onValue[0];
                if (value === undefined) {
                    value = dataItem;
                }
                valItem[fieldKey] = value;
            }

            if (this.validateRow(valItem)) {
	            dsData.push(valItem);
	        }
            this.indexRow(valItem);
		}
        return dsData;
    }
    
    validateRow(row) {
		for (let fieldKey of Object.keys(this.fieldMappings)) {
			let field = this.fieldMappings[fieldKey];
			if ((field.required || this.reqFields.indexOf(fieldKey) > -1) && ! row[fieldKey]) {
				return false;
			}
		}
		return true;
	}
    
    buildColorsByValue(fmtedData, propName) {
        this.colorsByValue = {};
        if (! propName) {
            propName = 'value';
        }
        for (let item of fmtedData) {
            this.colorsByValue[item[propName]] = item.color ? item.color : this.el.confValOrDefault('color', '#000000');
        }
    }
    
    colorByValue(value) {
        return this.colorsByValue[value] ? this.colorsByValue[value] : this.el.confValOrDefault('color', '#000000');
    }

}


import { Renderer } from '../../../sk-core/complets/renderer.js';

import { GridyElement, DS_REF_AN, PG_REF_AN, SORT_FIELD_AN } from "../gridy-element.js";


import { ResLoader } from "../../../sk-core/complets/res-loader.js";

import { ATTR_PLUGINS_AN, TPL_PATH_AN, IMPL_PATH_AN, TPL_VARS_AN } from "../../../sk-core/src/sk-element.js";
import { DIMPORTS_AN } from "../../../sk-core/src/sk-config.js";
import { PAGE_CHANGE_EVT } from "../pager/event/page-change-event.js";
import { DATA_LOAD_EVT } from "../datasource/event/data-load-event.js";

export class GridyChart extends GridyElement {

    //headers: boolean;

    //renderer: Renderer;
    //theme: string;

    //tpl;

    //bodyTplPath;
    //bodyTpl;

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    get implClassName() {
        return `GridyChart${this.capitalizeFirstLetter(this.theme)}`;
    }

    get implPath() {
        return this.getAttribute(IMPL_PATH_AN) ||
            `/node_modules/gridy-grid-${this.theme}/src/gridy-chart-${this.theme}.js`;
    }

    get tplPath() {
        return this.getAttribute(TPL_PATH_AN) || this.impl.tplPath;
    }

    set tplPath(tplPath) {
        this.setAttribute(TPL_PATH_AN, tplPath);
    }

    get tplVars() {
        if (! this._tplVars) {
            this._tplVars = this.hasAttribute(TPL_VARS_AN) ? JSON.parse(this.getAttribute(TPL_VARS_AN)) : {};
            Object.assign(this._tplVars, { width: 400, height: 200 });
        }
        return this._tplVars;
    }

    set tplVars(tplVars) {
        this._tplVars = tplVars;
    }

    get chartType() {
        return this.hasAttribute('type') ? this.getAttribute('type') : 'line';
    }

    get options() {
        if (! this._options) {
            try {
                this._options = this.hasAttribute('options') ? JSON.parse(this.getAttribute('options')) : null;
            } catch (e) {
                this._options = null;
            }
        }
        return this._options;
    }
    
    set options(options) {
        this._options = options;
        this.setAttribute('options', JSON.stringify(options));
    }

    set chartType(chartType) {
        this.setAttribute('type', chartType);
    }

    get driClassName() {
        let driCode = this.hasAttribute('dri')
            ? this.capitalizeFirstLetter(this.css2CamelCase(this.getAttribute('dri'))) : 'ChartJs';
        return `GridyChart${driCode}`;
    }

    get driPath() {
        let suffix = this.hasAttribute('dri')
            ? this.getAttribute('dri').replace(/-/g, '')
            : 'chartjs';
        return `/node_modules/gridy-chart-${suffix}/src/gridy-chart-${suffix}.js`;
    }
    
	get width() {
		return this.hasAttribute('width') ? parseInt(this.getAttribute('width')) : 400;
	}
	
	get height() {
		return this.hasAttribute('height') ? parseInt(this.getAttribute('height')) : 400;
	}
    
    get dsRef() {
        return this.hasAttribute(DS_REF_AN) ? this.getAttribute(DS_REF_AN) : this.getAttribute('dsref');
    }
    
    set dsRef(dsRef) {
        this.setAttribute(DS_REF_AN, dsRef);
    }

    get pgRef() {
        return this.getAttribute(PG_REF_AN) ? this.getAttribute(PG_REF_AN) : this.getAttribute('pgref');
    }
    
    set pgRef(pgRef) {
        this.setAttribute(PG_REF_AN, pgRef);
    }

    async dri() {
        if (! this._dri) {
            let forceInstance = this.el.hasAttribute('share-chart-dri') ? false : true;
            let dynImportsOff = (this.el.confValOrDefault(DIMPORTS_AN, null) === "false");
            this._dri = ResLoader.dynLoad(this.driClassName, this.driPath, function(def) {
                return new def(this);
            }.bind(this), forceInstance, false, false ,false, false, false, dynImportsOff);
        } 
        return this._dri; 
        
    }

    bindDataSource(dataSource) {
        this.logger.debug('GridyChart.bindDataSource()', arguments);
        if (dataSource) {
            this.dataSource = dataSource;
        }
        if (this.dataLoadedHandler) {
            this.dataSource.removeEventListener(DATA_LOAD_EVT, this.dataLoadedHandler);
        }
        this.dataLoadedHandler = function(event) {
            if (event.detail.totalResults) {

            }
            this.renderData(event);
            this.finishRender();
        }.bind(this);
        this.dataSource.addEventListener(DATA_LOAD_EVT, this.dataLoadedHandler);
    }

    get el() {
        return this;
    }

    async renderData(event) {
        this.logger.debug('GridyChart.renderData()', arguments);
        this.backPaged = event && event.detail.backPaged;
        if (! this.backPaged && this.getAttribute(SORT_FIELD_AN)) { // initial sort
            this.dataSource.sort(this.sortField, this.sortDirection);
        }
        let data = event && event.detail.data || this.dataSource.data;
        data = this.curPageData(data, this.dataSource.backPaged);
        let dri = await this.dri();
        this.chart = await dri.renderChart(this, data);
    }

    bootstrap() {
        super.bootstrap();
        this.logger.debug('GridyChart.bootstrap()', arguments);
        if (! this.dataSource) {
            if (this.dsRef && window[this.dsRef]) {
                if (typeof window[this.dsRef] === 'function') {
                    window[this.dsRef].call(this);
                } else {
                    let dsEl = window[this.dsRef];
                    this.dataSource = dsEl.dataSource;
                }
            } else {
                if (this.dsRef && typeof this.dsRef === 'string' && this.dsRef.indexOf('.') > 0) {
                    let pathTokens = this.dsRef.split('.');
                    let root = window[pathTokens[0]];
                    if (root) {
                        let ds = root[pathTokens[1]];
                        if (ds) {
                            this.dataSource = ds;
                        }
                    }
                }
            }
        }
        if (this.pgRef) {
            this.pager = window[this.pgRef];
        }
        if (this.pager) {
            this.pager.addEventListener(PAGE_CHANGE_EVT, this.onPageChanged.bind(this));
        }
        if (this.dataSource) {
            this.bindDataSource();
            if (this.dataSource.total > 0) {
                this.renderData().then(() => {
                    if (typeof this.finishReder === 'function') {
                        this.finishRender();
                    }
                });
            } else {
                this.dsLoadInitial();
            }
            
        }
    }

    connectedCallback() {
        this.logger.debug('GridyChart.connectedCallback()', arguments);
        this.curPageNum = 1;

        this.useConfigEl();
        Renderer.configureForElement(this);
        this.setupJsonPath();
        let attrPlugins = this.confValOrDefault(ATTR_PLUGINS_AN, false);
        if (attrPlugins) {
            this.bindAttrPlugins();
        }
        if (this.impl) { // if impl was cached render it
            this.impl.renderImpl();
        }

    }

    static get observedAttributes() {
        return [ 'options' ];
    }

}

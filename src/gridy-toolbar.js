

import { SkToolbar } from "../../sk-toolbar/src/sk-toolbar.js";

export class GridyToolbar extends SkToolbar {
	
	
	get itemTn() {
		return this.getAttribute('item-tn') || 'gridy-toolbar-item, sk-toolbar-item';
	}

}

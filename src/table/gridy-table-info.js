
import { Renderer } from '../../../sk-core/complets/renderer.js';
import { LANGS_BY_CODES } from "../gridy-config.js";

import { ATTR_PLUGINS_AN, TPL_PATH_AN, IMPL_PATH_AN } from '../../../sk-core/src/sk-element.js';

import { GridyElement, PER_PAGE_AN } from "../gridy-element.js";
import { FILTER_CHANGE_EVT } from "../filter/event/filter-change-event.js";
import { PAGE_CHANGE_EVT } from "../pager/event/page-change-event.js";
import { PER_PAGE_CHANGE_EVT } from "../perpage/event/per-page-change-event.js";
import { TableInfoRenderEvent } from "./event/table-info-render-event.js";
import { DATA_LOAD_EVT } from "../datasource/event/data-load-event.js";


export class GridyTableInfo extends GridyElement {

    //renderer: Renderer;
    //theme: string;

    //tplPath;
    //tpl;


    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    get implClassName() {
        return `GridyTableInfo${this.capitalizeFirstLetter(this.theme)}`;
    }

    get implPath() {
        return this.getAttribute(IMPL_PATH_AN) ||
            `/node_modules/gridy-grid-${this.theme}/src/gridy-table-info-${this.theme}.js`;
    }
    
    set implPath(path) {
        this.setAttribute(IMPL_PATH_AN, path);
    }

    changePage(event) {
        this.renderData(event);
    }

    changePerPage(event) {
        this.renderData(event);
    }

    onFilterChanged(event) {
        this.renderData(event);
    }

    get tplPath() {
        return this.getAttribute(TPL_PATH_AN) || this.impl.tplPath;
    }

    set tplPath(tplPath) {
        this.setAttribute(TPL_PATH_AN, tplPath);
    }

    get noEntriesTplPath() {
        return this.getAttribute('noent-tpl-path');
    }

    set noEntriesTplPath(tplPath) {
        this.setAttribute('noent-tpl-path', tplPath);
    }

    buildContext(event) {
        this.logger.debug('GridyTableInfo.buildContext()', arguments);
        let from = 0;
        let to = from + (this.dataSource ? this.dataSource.total : 0);
        let perPage;
        if (this.pager) {
            perPage = this.pager.perPage;
            from = ((this.pager.curPageNum - 1) * this.pager.perPage)
            to = from + perPage;
        } else {
            if (this.hasAttribute(PER_PAGE_AN)) {
                perPage = parseInt(this.getAttribute(PER_PAGE_AN));
                from = (event && event.detail.page) ? (event.detail.page - 1) * perPage : 0;
                to = from + perPage;
            } else {
                perPage = to;
            }
        }
        let total = 0;
        if (event && event.target && event.target.type === PAGE_CHANGE_EVT) {
            total = this.pager.totalResults || 0;
        } else if (event && event && event.type === FILTER_CHANGE_EVT) {
            to = (this.dataSource.total) < perPage ? this.dataSource.total : to;
            total = this.dataSource.total || 0;
        } else {
            if (event?.detail?.backPaged || this?.dataSource?.backPaged) {
                total = event?.detail.totalResults; // :TODO make configurable
            } else {
                total = this.dataSource ? this.dataSource.total : 0;
            }
        }
        if (to > total) {
            to = total;
        }
        return {
            from: total === 0 ? 0 : from + 1,
            to: to,
            total: total
        };
    }

    get langPath() {
        this.logger.debug('GridyTableInfo.langPath');
        if (! this._langPath) {
            if (this.configEl && this.configEl.hasAttribute('lang-full-path')) {
                this._langPath = `/${this.configEl.getAttribute('lang-full-path')}`;
            } else if (this.configEl && this.configEl.hasAttribute('lang-path')) {
                let locale = LANGS_BY_CODES[this.lang];
                if (locale === 'RU') {
                    this._langPath = `${this.configEl.getAttribute('lang-path')}/gridy-i18n-ru_RU.json`;
                }
            } else {
                let locale = LANGS_BY_CODES[this.lang];
                if (locale === 'RU') {
                    this._langPath = `/node_modules/gridy-grid-${this.theme}/src/i18n/gridy-i18n-ru_RU.json`;
                }
            }
        }
        return this._langPath;
    }

    renderData(event) {
        this.logger.debug('GridyTableInfo.renderData()', arguments);
        let path = this.tplPath;
        let id = 'tableInfoTpl';
        if (this.dataSource && this.dataSource.total === 0 && (this.noEntriesTplPath || this.querySelector('#noEntriesInfoTpl'))) {
            path = this.noEntriesTplPath;
            id = 'noEntriesInfoTpl';
        }
        let context = this;
        this.renderer.mountTemplate(path, id, this, { ...context, ...this.tplVars }).then((templateEl) => {
            this.tpl = templateEl;
            let el = this.renderer.prepareTemplate(templateEl);
            let map = this.buildContext(event);
            let path = null;
            if (this.lang) {
                path = this.langPath;
                if (path && map.from && map.to && map.total) {
                    this.renderer.localizeEl(el, this.lang, path, this.constructor.name).then((translatedEl) => {
                        this.innerHTML = '';
                        this.insertAdjacentHTML('beforeend', this.renderer.templateEngine.render(translatedEl, map));
                        this.dispatchEvent(new TableInfoRenderEvent());
                    });
                }
            }
            if (! this.lang || ! path && map.from && map.to && map.total) {
                this.innerHTML = '';
                this.insertAdjacentHTML('beforeend', this.renderer.templateEngine.render(el, map));
                this.dispatchEvent(new TableInfoRenderEvent());
            }
        });
    }

    bootstrap() {
        super.bootstrap();
        this.logger.debug('GridyTableInfo.bootstrap()', arguments);
        if (this.dataSource) {
            this.dataSource.addEventListener(DATA_LOAD_EVT, this.renderData.bind(this));
        }
        if (this.pager) {
            this.pager.addEventListener(PAGE_CHANGE_EVT, this.changePage.bind(this));
        }
        this.addEventListener(PAGE_CHANGE_EVT, this.changePage.bind(this));
        if (this.perPageEl) {
            this.perPageEl.addEventListener(PER_PAGE_CHANGE_EVT, this.changePerPage.bind(this));
        }
        this.addEventListener(PER_PAGE_CHANGE_EVT, this.changePerPage.bind(this));
        this.addEventListener(FILTER_CHANGE_EVT, this.onFilterChanged.bind(this));
    }

    connectedCallback() {
        this.logger.debug('GridyTableInfo.connectedCallback()', arguments);
        this.useConfigEl();
        this.setAttribute('basepath', this.getAttribute('base-path')); // compatibility fix
        Renderer.configureForElement(this);
        let attrPlugins = this.confValOrDefault(ATTR_PLUGINS_AN, false);
        if (attrPlugins) {
            this.bindAttrPlugins();
        }
        if (this.impl) { // if impl was cached render it
            this.impl.renderImpl();
        }
    }
    
    clear() {
        this.innerHTML = '';
    }
}

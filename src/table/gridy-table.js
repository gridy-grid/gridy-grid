
import { Renderer } from "../../../sk-core/complets/renderer.js";

import { JsonPath, JSONPATH_CN, JSONPATH_PT } from "../../../sk-core/src/json-path.js";


import { GridyDataSource, DATA_SOURCE_TN } from "./../datasource/gridy-data-source.js";

import { GridyPager } from "./../pager/gridy-pager.js";

import { DataSourceLocal } from "../datasource/data-source-local.js";

import {ATTR_PLUGINS_AN, TPL_PATH_AN, IMPL_PATH_AN, DISABLED_AN} from "../../../sk-core/src/sk-element.js";

import {
    GridyElement, PER_PAGE_DEFAULT, CUR_PAGE_NUM_AN, PER_PAGE_AN,
    PG_REF_AN, DS_REF_AN, SORT_FIELD_AN
} from "../gridy-element.js";

import { RowClickEvent } from "./event/row-click-event.js";
import { HeadRowRenderEvent } from "./event/head-row-render-event.js";
import { HeadRenderEvent } from "./event/head-render-event.js";
import { BodyRowRenderEvent } from "./event/body-row-render-event.js";
import { BodyRenderEvent } from "./event/body-render-event.js";
import { FILTER_CHANGE_EVT, FilterChangeEvent } from "../filter/event/filter-change-event.js";
import { PAGE_CHANGE_EVT, PageChangeEvent } from "../pager/event/page-change-event.js";
import { PER_PAGE_CHANGE_EVT, PerPageChangeEvent } from "../perpage/event/per-page-change-event.js";
import { DATA_LOAD_EVT } from "../datasource/event/data-load-event.js";
import { FootRowRenderEvent } from "./event/foot-row-render-event.js";
import { FootRenderEvent } from "./event/foot-render-event.js";


import { FIELD_TITLE_AN } from "../filter/gridy-filter.js";
import { ResLoader } from "../../../sk-core/complets/res-loader.js";


export const ROW_OPENED_CN = "gridy-row-opened";
export const PAGER_TN = "gridy-pager";



export const HEADERS_AN = "headers";
export const SHOW_HEADERS_AN = "show-headers";
export const SHOW_FOOTERS_AN = "show-footers";

export const HEAD_TPL_PATH_AN = "head-tpl-path";
export const BODY_TPL_PATH_AN = "body-tpl-path";
export const FOOT_TPL_PATH_AN = "foot-tpl-path";
export const ROW_PANE_TPL_PATH_AN = "row-pane-tpl-path";
export const PAGER_TN_AN = "pager-tn";
export const DATA_SOURCE_TN_AN = "data-source-tn";


export const TABLE_TN_AN = "table-tn";
export const TABLE_TN_DEFAULT = "table";
export const TABLE_HEAD_TN_AN = "head-tn";
export const TABLE_HEAD_TN_DEFAULT = "thead";
export const TABLE_FOOT_TN_AN = "foot-tn";
export const TABLE_FOOT_TN_DEFAULT = "tfoot";
export const TABLE_BODY_TN_AN = "body-tn";
export const TABLE_BODY_TN_DEFAULT = "tbody";
export const TABLE_HEAD_CELL_TN_AN = "hcell-tn";
export const TABLE_HEAD_CELL_TN_DEFAULT = "th";
export const TABLE_ROW_TN_AN = "row-tn";
export const TABLE_ROW_TN_DEFAULT = "tr";
export const TABLE_CELL_TN_AN = "cell-tn";
export const TABLE_CELL_TN_DEFAULT = "td";

export const TABLE_ROW_STATE_AN = "row-state";
export const HEAD_IN_FOOT_AN = "head-in-foot";

export const TABLE_ROW_HASHCODE_AN = "data-hash-code"
export const TABLE_CALC_CACHE_AN = "table-calc-cache";
export const TABLE_FMT_CACHE_AN = "table-fmt-cache";

export const TABLE_ROW_PANE_AN = "row-pane";
export const TABLE_LANG_DEFAULT = "en";
export const TABLE_I18N = {
    'ru': {
        'Columns': 'Колонки'
    },
    'en': {
        'Columns': 'Columns'
    },
    'de': {
        'Columns': 'Tabellenspalten'
    },
    'cn': {
        'Columns': '表列'
    },
    'hi': {
        'Columns': 'टेबल कॉलम'
    }
}


export class GridyTable extends GridyElement {

    //headers: boolean;

    //renderer: Renderer;
    //theme: string;

    //pager: GridyPager;

    //tableEl;
    //bodyEl;
    //headTplPath;
    //headTpl;
    //bodyTplPath;
    //bodyTpl;

    //renderTimest: number;


    constructor() {
        super();
        this.renderTimest = -1;
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    get implClassName() {
        return `GridyTable${this.capitalizeFirstLetter(this.theme)}`;
    }

    get implPath() {
        return this.getAttribute(IMPL_PATH_AN) ||
            `/node_modules/gridy-grid-${this.theme}/src/gridy-table-${this.theme}.js`;
    }

    get showHeaders() {
        if (this.getAttribute(HEADERS_AN) === 'false' || this.getAttribute(SHOW_HEADERS_AN) === 'false') {
            return false;
        }
        return true;
    }
    
    get showFooters() {
        if (this.hasAttribute(SHOW_FOOTERS_AN) && this.getAttribute(SHOW_FOOTERS_AN) !== 'false') {
            return true;
        }
        return false;
    }

    set headers(headers) {
        this.setAttribute(HEADERS_AN, headers);
    }

    get tplPath() {
        return this.getAttribute(TPL_PATH_AN) || this.impl.tplPath;
    }

    set tplPath(tplPath) {
        this.setAttribute(TPL_PATH_AN, tplPath);
    }

    get headTplPath() {
        return this.getAttribute(HEAD_TPL_PATH_AN) || this.impl.headTplPath;
    }

    set headTplPath(tplPath) {
        this.setAttribute(HEAD_TPL_PATH_AN, tplPath);
    }
    
    get footTplPath() {
        return this.getAttribute(FOOT_TPL_PATH_AN) || this.impl.footTplPath;
    }
    
    set footTplPath(tplPath) {
        this.setAttribute(FOOT_TPL_PATH_AN, tplPath);
    }
    
    get bodyTplPath() {
        return this.getAttribute(BODY_TPL_PATH_AN) || this.impl.bodyTplPath;
    }

    set bodyTplPath(tplPath) {
        this.setAttribute(BODY_TPL_PATH_AN, tplPath);
    }

    get rowPaneTplPath() {
        return this.getAttribute(ROW_PANE_TPL_PATH_AN) || this.impl.rowPaneTplPath;
    }

    set rowPaneTplPath(tplPath) {
        this.setAttribute(ROW_PANE_TPL_PATH_AN, tplPath);
    }

    get tableEl() {
        if (! this._tableEl) {
            this._tableEl = this.querySelector('table');
        }
        return this._tableEl;
    }

    get pagerTn() {
        return this.getAttribute(PAGER_TN_AN) || PAGER_TN;
    }

    set pagerTn(pagerTn) {
        return this.setAttribute(PAGER_TN_AN, pagerTn);
    }

    get dataSourceTn() {
        return this.getAttribute(DATA_SOURCE_TN_AN) || DATA_SOURCE_TN;
    }

    set dataSourceTn(dataSourceTn) {
        return this.setAttribute(DATA_SOURCE_TN_AN, dataSourceTn);
    }
    
    get jsonPath() {
        if (! this._jsonPath) {
            this._jsonPath = ResLoader.dynLoad(JSONPATH_CN, JSONPATH_PT, function(def) {
                return new def();
            }.bind(this), false, false, false ,false, false, false, true, true, JsonPath);
        }
        return this._jsonPath;
    }

    set jsonPath(jsonPath) {
        this._jsonPath = jsonPath;
    }
    
    get tableTn() {
        return this.getAttribute(TABLE_TN_AN) || TABLE_TN_DEFAULT;
    }
    
    set tableTn(tableTn) {
        return this.setAttribute(TABLE_TN_AN, tableTn);
    }
    
    get headTn() {
        return this.getAttribute(TABLE_HEAD_TN_AN) || TABLE_HEAD_TN_DEFAULT;
    }
    
    set headTn(headTn) {
        return this.setAttribute(TABLE_HEAD_TN_AN, headTn);
    }
    
    get footTn() {
        return this.getAttribute(TABLE_FOOT_TN_AN) || TABLE_FOOT_TN_DEFAULT;
    }
    
    set footTn(footTn) {
        return this.setAttribute(TABLE_FOOT_TN_AN, footTn);
    }
    
    get headCellTn() {
        return this.getAttribute(TABLE_HEAD_CELL_TN_AN) || TABLE_HEAD_CELL_TN_DEFAULT;
    }
    
    set headCellTn(cellTn) {
        return this.setAttribute(TABLE_HEAD_CELL_TN_AN, cellTn);
    }
    
    get bodyTn() {
        return this.getAttribute(TABLE_BODY_TN_AN) || TABLE_BODY_TN_DEFAULT;
    }
    
    set bodyTn(bodyTn) {
        return this.setAttribute(TABLE_BODY_TN_AN, bodyTn);
    }
    
    get rowTn() {
        return this.getAttribute(TABLE_ROW_TN_AN) || TABLE_ROW_TN_DEFAULT;
    }
    
    set rowTn(rowTn) {
        return this.setAttribute(TABLE_ROW_TN_AN, rowTn);
    }
    
    get cellTn() {
        return this.getAttribute(TABLE_CELL_TN_AN) || TABLE_CELL_TN_DEFAULT;
    }
    
    set cellTn(cellTn) {
        return this.setAttribute(TABLE_CELL_TN_AN, cellTn);
    }
    
    get selectedRows() {
        return this.selectedItems;
    }
    
    set selectedRows(rows) {
        this.selectedItems = rows;
    }
    
    get selectedRow() {
        return this.selectedItem;
    }
    
    set selectedRow(row) {
        this.selectedItem = row;
    }
    
    get renderedRows() {
        if (! this._renderedRows) {
            this._renderedRows = new Map();
        }
        return this._renderedRows;
    }
    
    set renderedRows(rows) {
        this._renderedRows = rows;
    }
    
    get rowsMeta() {
        if (! this._rowsMeta) {
            this._rowsMeta = new Map();
        }
        return this._rowsMeta;
    }
    
    set rowsMeta(rowsMeta) {
        this._rowsMeta = rowsMeta;
    }
    
    get tplClones() {
        if (! this._tplClones) {
            this._tplClones = [];
        }
        return this._tplClones;
    }
    
    set tplClones(tplClones) {
        this._tplClones = tplClones;
    }
    
    rowByDataItem(dataItem) {
        let hashCode = this.hashDataItem(dataItem);
        return this.renderedRows.get(hashCode);
    }
    
    recRenderedRow(dataItem, trEl) {
        let hashCode = this.hashDataItem(dataItem);
        trEl.setAttribute(TABLE_ROW_HASHCODE_AN, hashCode);
        this.renderedRows.set(hashCode, trEl);
        return hashCode;
    }
    
    clearSortFromCells() {
        let sortClasses = ['sort', 'sort-asc', 'sort-desc'];
        if (this.showHeaders) {
            let headCells = this.querySelectorAll(this.headTn + ' ' + this.rowTn + ' ' + this.headCellTn);
            headCells.forEach((cell) => {
                cell.classList.remove(...sortClasses);
            });
        }
        if (this.hasAttribute(HEAD_IN_FOOT_AN)) {
            let footRowCells = this.querySelectorAll(this.footTn + ' ' + this.rowTn + ' ' + this.headCellTn);
            footRowCells.forEach((cell) => {
                cell.classList.remove(...sortClasses);
            });
        }
    }
    
    markCellsSort(targets) {
        for (let target of targets) {
            target.classList.remove('sort-asc', 'sort-desc');
            target.classList.add('sort');
            if (this.sortDirection === 'asc') {
                target.classList.add('sort-asc');
            } else {
                target.classList.add('sort-desc');
            }
        }
    }
    
    isCellInHead(cell) {
        return (cell.parentElement.parentElement.tagName === this.headTn.toUpperCase());
    }
    
    changeSort(event) {
        this.logger.debug('GridyTable.changeSort()', arguments);
        if (this.disabled) {
            return false;
        }
        let params = {};
        let oldSortField = this.sortField;
        let newSortField = event.target.dataset.sort;
        if (oldSortField === newSortField) {
            this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
        } else {
            this.sortDirection = 'asc';
        }
        this.sortField = newSortField;

        if (this.backPaged || this.dataSource.backPaged) {
            let sortField = this.guessFieldId(this.sortField);
            let filterValues = this.buildFilterValues();
            if (filterValues.length > 0) {
                params.values = filterValues;
            }
            this.dataSource.clearData();
            this.dataSource.fetchData(Object.assign({
                backPaged: true, page: this.pager.curPageNum,
                sortField: sortField, sortDirection: this.sortDirection
            }, params)).then((detail) => {
                this.dataSource.loadData(null, detail);
            });
        } else {
            this.dataSource.sort(this.sortField, this.sortDirection);
            // remove prev sort markers
        }
        this.clearSortFromCells();
        if (event && event.target) {
            let targets = [event.target];
            let sortName = event.target.dataset.sort;
            let additionalTarget = null;
            if (this.isCellInHead(event.target)) {
                additionalTarget = this.querySelector( `${this.footTn} [data-sort='${sortName}']`);
            } else {
                additionalTarget = this.querySelector( `${this.headTn} [data-sort='${sortName}']`);
            }
            if (additionalTarget) {
                targets.push(additionalTarget);
            }
            this.markCellsSort(targets);
        }
        if (this.dataSource.dataFiltered) {
            this.renderBody(this.dataSource.dataFiltered);
        } else {
            this.renderBody(this.dataSource.data);
        }
    }
    
    restoreParams() {
        let params = {};
        if (this.sortField) {
            params.sortDirection = this.sortDirection ? this.sortDirection : 'asc';
            params.sortField = this.sortField;
        }
        if (this.pager) {
            params.page = this.curPageNum;
            params.perPage = this.perPage;
        }
        return params;
    }
    
    buildFilterValues() {
        let filterValues = [];
        for (let filter of this.filters) {
            if (filter.filters) {
                filterValues = filterValues.concat(filter.filters);
            } else {
                let title = filter.hasAttribute(FIELD_TITLE_AN) ? filter.getAttribute(FIELD_TITLE_AN) : undefined;
                if (filter.value != null) {
                    filterValues.push(title ? { title: title, mode: filter.mode, value: filter.value, ctx: filter } : filter.value);
                }
            }
        }
        return filterValues;
    }

    changeFilter(event) {
        this.logger.debug('GridyTable.changeFilter()', arguments);
        let params = {};
        if (this.backPaged || this.dataSource.backPaged) {
            params = this.restoreParams();
        }
        if (this.filters && this.filters.length > 0) {
            let filterValues = this.buildFilterValues();
            this.dataSource.filter(filterValues, null, null, params);
        } else {
            let fieldTitle = undefined;
            if (event.detail.field)  {
                fieldTitle = event.detail.field;
            }
            let mode = undefined;
            if (event.detail.mode) {
                mode = event.detail.mode;
            }
            this.dataSource.filter(event.detail.value, fieldTitle, mode, params);
        }
        this.renderBody(this.dataSource.dataFiltered);
        if (this.pager) {
            this.pager.dispatchEvent(new FilterChangeEvent({
                detail: { value: event.target.value }
            }));
        }
    }

    bindHeadingEvents(row) {
        this.logger.debug('GridyTable.bindHeadingEvents()', arguments);
        if (this.onhrowclick && typeof this.onhrowclick === 'function') {
            row.addEventListener('click', this.onhrowclick.bind(this));
        }
    }

    get curPageNum() {
        let pageNum = parseInt(this.getAttribute(CUR_PAGE_NUM_AN)) || 1;
        if (this.pager) {
            pageNum = this.pager.curPageNum;
        }
        return pageNum;
    }

    set curPageNum(pageNum) {
        this.setAttribute(CUR_PAGE_NUM_AN, pageNum);
    }
    
    get calcCache() {
        if (! this._calcCache) {
            this._calcCache = new Map();
        }
        return this._calcCache;
    }
    
    set calcCache(calcCache) {
        this._calcCache = calcCache;
    }
    
    get calcCacheEnabled() {
        if (! this._calcCacheEnabled) {
            this._calcCacheEnabled = this.confValEnabled(TABLE_CALC_CACHE_AN);
        }
        return this._calcCacheEnabled;
    }
    
    get fmtCacheEnabled() {
        if (! this._fmtCacheEnabled) {
            this._fmtCacheEnabled = this.confValEnabled(TABLE_FMT_CACHE_AN);
        }
        return this._fmtCacheEnabled;
    }
    
    get fmtCache() {
        if (! this._fmtCache) {
            this._fmtCache = new Map();
        }
        return this._fmtCache;
    }
    
    set fmtCache(fmtCache) {
        this._fmtCache = fmtCache;
    }

    async renderHeaders() {
        this.logger.debug('GridyTable.renderHeaders()', arguments);
        let context = this;
        return new Promise(async (resolve) => {
            this.headTpl = await this.renderer.mountTemplate(this.headTplPath, 'tableHeadTpl', this, { ...context, ...this.tplVars });
            //this.renderHeaderContents();

            this.headEl = this.renderer.prepareTemplate(this.headTpl);

            let row = this.headEl.querySelector(this.rowTn) || this.querySelector(this.headTn + ' ' + this.rowTn)
                || this.renderer.createEl(this.rowTn);
            row.innerHTML = '';
            this.bindHeadingEvents(row);
            let sortField = this.getAttribute(SORT_FIELD_AN);
            this.renderColumnNameCells(row, sortField);
            if (sortField) {
                if (this.headRowHandler) {
                    row.removeEventListener('click', this.headRowHandler);
                }
                this.headRowHandler = this.changeSort.bind(this);
                row.addEventListener('click', this.headRowHandler);
            }
            let tableEl = this.querySelector(this.tableTn);
            if (tableEl !== null) {
                let headEl = tableEl.querySelector(this.headTn);
                if (! headEl) {
                    headEl = (this.headEl.tagName === this.headTn.toUpperCase()) ? this.headEl : this.headEl.querySelector(this.headTn);
                }
                headEl.innerHTML = '';
                if (headEl.tagName === this.headTn.toUpperCase()) {
                    headEl.appendChild(row);
                    this.dispatchEvent(new CustomEvent('headerRowRendered')); // :DEPRECATED
                    this.dispatchEvent(new HeadRowRenderEvent());
                } else {
                    if (!headEl && tableEl) {
                        headEl = this.renderer.createEl(this.headTn);
                        this.tableEl.appendChild(headEl);
                    }
                    headEl.appendChild(row);
                }
                if (headEl != null) {
                    this.headEl = headEl;
                }
                tableEl.appendChild(this.headEl);
                this.dispatchEvent(new CustomEvent('headersRendered')); // :DEPRECATED
                this.dispatchEvent(new HeadRenderEvent());
            } else {
                console.error('Not found table el for header rendering');
            }
            resolve();
        });

    }
    
    fieldsByStoredCfg() {
        let fields = [];
        if (this.storedCfg.columnsOrder) {
            for (let fieldName of this.storedCfg.columnsOrder) {
                let field = this.dataSource.findField(fieldName, this.dataSource.fields);
                fields.push(field);
            }
        } else {
            fields = this.dataSource.fields;
        }
        return fields;
    }
    
    renderColumnNameCells(row, sortField) {
        let fields = this.fieldsByStoredCfg();
        for (let field of fields) {
            if (field.noRender) {
                continue;
            }
            let headCell = this.renderer.createEl(this.headCellTn);
            if (typeof field == 'string') {
                headCell.textContent = field;
                headCell.setAttribute('data-sort', field);
            } else {
                if (field.title !== undefined) {
                    headCell.textContent = field.displayTitle ? field.displayTitle : field.title;
                    let cellField = this.dataSource.findField(field.path || field.title);
                    headCell.setAttribute('data-sort', cellField.path || cellField.name || cellField.title);
                }
            }
            // initial sort marker classes
            if (sortField && (field.path === sortField || field === sortField)) {
                headCell.classList.add('sort');
                if (this.sortDirection === 'desc') {
                    headCell.classList.add('sort-desc');
                } else {
                    headCell.classList.add('sort-asc');
                }
            }
            this.applyColumnsCfg(field, headCell);
            row.appendChild(headCell);
        }
    }
    
    applyColumnsCfg(field, cell) {
        let fieldId = this.guessFieldId(field.path);
        let cfg = this.storedCfg.columns && this.storedCfg.columns[fieldId] ? this.storedCfg.columns[fieldId] : {};
        if (cfg.style) {
            for (let propName of Object.keys(cfg.style)) {
                cell.style[propName] = cfg.style[propName];
            }
        }
        if (cfg.display === "none" || cfg.display === false) {
            cell.style.display = "none";
        }
        if (cfg.attr) {
            for (let attrName of Object.keys(cfg.attr)) {
                cell.setAttribute(attrName, cfg.attr[attrName]);
            }
        }
        
    }

    flushOldFooter() {
        let oldFooter = this.querySelector(this.footTn);
        if (oldFooter && oldFooter.parentElement) {
            oldFooter.parentElement.removeChild(oldFooter);
        }
    }
    
    async renderFooters() {
        this.logger.debug('GridyTable.renderFooters()', arguments);
        let context = this;
        this.footTpl = await this.renderer.mountTemplate(this.footTplPath, 'tableFootTpl', this,
            { ...context, ...this.tplVars });
        return new Promise((resolve) => {
            this.flushOldFooter();
            this.footEl = this.renderer.prepareTemplate(this.footTpl);
            let row = this.footEl.querySelector(this.rowTn) || this.querySelector(this.footTn + ' ' + this.rowTn)
                || this.renderer.createEl(this.rowTn);
            this.bindHeadingEvents(row);
            if (this.hasAttribute(HEAD_IN_FOOT_AN)) {
                let sortField = this.getAttribute(SORT_FIELD_AN);
                this.renderColumnNameCells(row, sortField);
                if (sortField) {
                    if (this.footRowHandler) {
                        row.removeEventListener('click', this.footRowHandler);
                    }
                    this.footRowHandler = this.changeSort.bind(this);
                    row.addEventListener('click', this.footRowHandler);
                }
            }
            let tableEl = this.querySelector(this.tableTn);
            if (tableEl !== null) {
                let footEl = tableEl.querySelector(this.footTn)
                || (this.footEl.tagName === this.footTn.toUpperCase()) ? this.footEl : this.footEl.querySelector(this.footTn);
                footEl.innerHTML = '';
                if (footEl.tagName === this.footTn.toUpperCase()) {
                    footEl.appendChild(row);
                } else {
                    if (!footEl && tableEl) {
                        footEl = this.renderer.createEl(this.footTn);
                        this.tableEl.appendChild(footEl);
                    }
                    footEl.appendChild(row);
                    this.dispatchEvent(new FootRowRenderEvent());
                }
                if (footEl != null) {
                    this.footEl = footEl;
                }
                tableEl.appendChild(this.footEl);
                this.dispatchEvent(new FootRenderEvent());
            } else {
                console.error('Not found table el for footer rendering');
            }
            resolve();
        });
        
    }

    routeRowClick(event, row) {
        if (event.button === 2) { // right button click
            if ((event.target && event.target.classList.contains('no-rowrclick'))
                || (event.srcElement && event.srcElement.classList.contains('no-rowrclick'))) {
                return false;
            }
            if (this.onrowrclick && typeof this.onrowrclick === 'function') {
                event.preventDefault();
                this.onrowrclick.call(this, event, row);
                return false;
            }
        } else {
            if ((event.target && event.target.classList.contains('no-rowclick'))
                || (event.srcElement && event.srcElement.classList.contains('no-rowclick'))) {
                return false;
            }
            /*if (this.hasAttribute(SELECTABLE_AN)) {
                this.toggleRowSelection(row);
            }*/
            if (this.onrowclick && typeof this.onrowclick === 'function') {
                this.onrowclick.call(this, event, row);
            }
            if (this.hasAttribute('click-pane') || this.hasAttribute(TABLE_ROW_PANE_AN)) {
                this.onClickPaneClick.call(this, event);
            }
        }
        this.dispatchEvent(new RowClickEvent({
            detail: { ... event.detail, row: row, button: event.button, origEvent: event },
            bubbles: true,
            composed: true
        }));
    }

    bindRowEvents(row) {
        this.logger.debug('GridyTable.bindRowEvents()', arguments);

        if (row.rowClickHandler) {
            row.removeEventListener('mousedown', row.rowClickHandler);
        }
        row.rowClickHandler = row.addEventListener('mousedown', function(event) {
            this.routeRowClick(event, row);
        }.bind(this));
        this.bindSelectable(row);
    }

    onClickPaneClick(event, rclick) {
        let cell = event.target;
        let row = cell.parentElement;
        if (row.classList.contains(ROW_OPENED_CN)) {
            let pane = row.nextElementSibling;
            if (pane != null) {
                pane.parentElement.removeChild(pane);
                row.classList.remove(ROW_OPENED_CN);
                if (this.hasAttribute(TABLE_ROW_STATE_AN)) {
                    let itemHash = this.hashDataItem(row.dataItem);
                    this.rowsMeta[itemHash] = { open: false };
                }
            }
        } else {
            let context = this;
            let rowPaneTplLoad = this.renderer.mountTemplate(this.rowPaneTplPath, 'tableRowPaneTpl', this, { ...context, ...this.tplVars });
            rowPaneTplLoad.then(function(tpl) {
                this.renderRowPane(row, tpl);
                if (this.hasAttribute(TABLE_ROW_STATE_AN)) {
                    let itemHash = this.hashDataItem(row.dataItem);
                    this.rowsMeta[itemHash] = { open: true };
                }
            }.bind(this));
        }
    }

    renderRowPane(row, tpl) {
        let prepared = this.renderer.prepareTemplate(tpl);
        let rowContext = row.dataItem ? { ...row.dataItem } : {};
        let rendered = this.renderer.renderMustacheVars(prepared, rowContext);
        let paneRow = this.renderer.createEl(this.rowTn);
        paneRow.insertAdjacentHTML('beforeend', rendered);
        if (row.parentNode) {
            row.parentNode.insertBefore(paneRow, row.nextSibling);
        }
        row.classList.add(ROW_OPENED_CN);
    }

    hashDataItem(item) {
        return this.dataSource.hashDataItem(item);
    }

    renderRow(bodyEl, dataItem) {
        this.logger.debug('GridyTable.renderRow()', arguments);
        let row = this.renderer.createEl(this.rowTn);

        this.bindRowEvents(row);
    
        row.dataItem = dataItem;
        let dataItemHashCode = this.recRenderedRow(dataItem, row);
        let fields = this.fieldsByStoredCfg();
        for (let field of fields) {
            if (field.noRender) {
                continue;
            }
    
            let cell = this.renderer.createEl(this.cellTn);
            let value = this.specCellContents(field, dataItem, cell, dataItemHashCode);
            this.specCellAtts(field, cell, value, dataItem);
    
            this.specRowAttrs(field, row, value, dataItem);
            this.applyColumnsCfg(field, cell);
            row.appendChild(cell);
        }
        let rowInTable = bodyEl.appendChild(row);
        if (this.hasAttribute(TABLE_ROW_STATE_AN) && this.getAttribute(TABLE_ROW_STATE_AN) !== 'false') {
            let itemHash = this.hashDataItem(row.dataItem);
            let itemMeta = this.rowsMeta[itemHash];
            if (itemMeta && itemMeta.open === true) {
                let context = this;
                let onPaneTplLoad = this.renderer.mountTemplate(this.rowPaneTplPath, 'clickPaneTpl', this, { ...context, ...this.tplVars, ...dataItem });
                onPaneTplLoad.then((tpl) => {
                    this.renderRowPane(rowInTable, tpl);
                });
            }
        }
        this.restoreRowSelection(row, dataItem);
        this.dispatchEvent(new CustomEvent('bodyRowRendered')); // :DEPRECATED
        this.dispatchEvent(new BodyRowRenderEvent());
    }
    
    restoreRowSelection(row, dataItem) {
        if (this.isMultiSelect) {
            for (let selectedRow of this.selectedRows) {
                if (this.rowsEqual(selectedRow, { dataItem: dataItem })) {
                    this.toggleRowSelection(row);
                }
            }
        } else {
            if (this.selectedRow) {
                if (this.rowsEqual(this.selectedRow, { dataItem: dataItem })) {
                    this.toggleRowSelection(row);
                }
            }
        }
    }
    

    specCellContents(field, dataItem, cell, dataItemHashCode) {
        this.logger.debug('GridyTable.specCellContents()', arguments);
        let value = '', rendered = '';
        if (typeof field == 'string') {
            value = dataItem[field];
        } else {
            if (field.path && field.path.startsWith('$')) {
                let onValue = this.jsonPath.query(dataItem, field.path);
                value = onValue[0];
            } else {
                value = field.path ? dataItem[field.path] : dataItem[field.name.toLowerCase()]
                    || dataItem[field.name] || undefined;
            }
            if (value === undefined) {
                value = dataItem;
            }
            if (field.tplPath) {
                if (this.impl.rowTpls.has(field.title)) {
                    let tpl = this.impl.rowTpls.get(field.title);
                    let prepared = this.renderer.prepareTemplate(tpl);
                    rendered = value = this.renderer.renderMustacheVars(prepared, {
                        value: value,
                        ...dataItem
                    });
                } else {
                    this.logger.warn(`row ${field.title} was not preloaded`);
                }
            }
            let calcs = {};
            if (field.calc) {
                if (dataItemHashCode && this.calcCacheEnabled && this.calcCache.get(dataItemHashCode)) {
                    calcs = this.calcCache.get(dataItemHashCode);
                } else {
                    calcs = this.dataSource.calcRow(field.calc, dataItem);
                    if (dataItemHashCode && this.calcCacheEnabled) {
                        this.calcCache.set(dataItemHashCode, calcs);
                    }
                }
            }
            if (field.fmt) {
                calcs = typeof calcs === 'object' ? calcs : { calc: calcs };
                if (dataItemHashCode && this.fmtCacheEnabled && this.fmtCache.get(dataItemHashCode)) {
                    value = this.fmtCache.get(dataItemHashCode);
                } else {
                    if (typeof field.fmt === 'function') {
                        value = field.fmt.call(this, field, value, dataItem, rendered);
                    } else if (typeof field.fmt === 'string') {
                        if (field.fmt.startsWith('return')) {
                            try {
                                value = (Function(field.fmt).call(this, {
                                    value: value, ...dataItem, ...calcs,
                                    rendered: rendered
                                }));
                            } catch (e) {
                                this.logger.warn(`Error calling fmt code ${field.fmt} for field ${JSON.stringify(field)} and dataItem ${JSON.stringify(dataItem)}`, e);
                            }
                        } else {
                            value = this.renderer.renderMustacheVars(field.fmt, {
                                value: value, ...dataItem, ...calcs,
                                rendered: rendered
                            });
                        }
                    }
                    if (dataItemHashCode && this.fmtCacheEnabled) {
                        this.fmtCache.set(dataItemHashCode, value);
                    }
                }
                
            }
        }
        if (value && !field.html) {
            cell.textContent = value;
        }
        if (value && field.html) {
            cell.innerHTML = value;
        }
        return value;
    }

    specCellAtts(field, cell, value, dataItem) {
        this.logger.debug('GridyTable.specCellAtts()', arguments);
        if (field.attr) {
            for (let attrName of Object.keys(field.attr)) {
                let attrValue = field.attr[attrName];
                if (typeof attrValue == 'string') {
                    cell.setAttribute(attrName, attrValue);
                } else if (typeof attrValue == 'function') {
                    cell.setAttribute(attrName, attrValue.call(this, field, value, dataItem));
                }
            }
        }
    }

    specRowAttrs(field, row, value, dataItem) {
        this.logger.debug('GridyTable.specRowAttrs()', arguments);
        if (field.rowattr) {
            for (let attrName of Object.keys(field.rowattr)) {
                let attrValue = field.rowattr[attrName];
                if (typeof attrValue == 'string') {
                    row.setAttribute(attrName, attrValue);
                } else if (typeof attrValue == 'function') {
                    row.setAttribute(attrName, attrValue.call(this, field, value, dataItem));
                }
            }
        }
    }

    async render(data) {
        this.logger.debug('GridyTable.render()', arguments);
        let context = this;
        let onTplMounted = this.renderer.mountTemplate(this.tplPath, 'tableTpl', this, { ...context, ...this.tplVars });
        onTplMounted.then((tpl) => {
            this.tpl = tpl;
            this.storeTpls();
            this.innerHTML = '';
            let el = this.renderer.prepareTemplate(this.tpl);
            this.appendChild(el);
            if (this.dataSource && this.showHeaders) {
                let onHeadersRendered = this.renderHeaders();
                onHeadersRendered.then(() => {
                    let onBodyRendered = this.renderBody(data, this.dataSource.backPaged || this.backPaged);
                    onBodyRendered.then(() => {
                        if (this.hasAttribute(HEAD_IN_FOOT_AN) || this.showFooters) {
                            let onFooterRendered = this.renderFooters();
                            onFooterRendered.then(() => {
                                this.finishRender();
                            })
                        } else {
                            this.finishRender();
                        }
                    });
                });
            } else {
                let onBodyRendered = this.renderBody(data, this.backPaged);
                onBodyRendered.then(() => {
                    if (this.hasAttribute(HEAD_IN_FOOT_AN) || this.showFooters) {
                        let onFooterRendered = this.renderFooters();
                        onFooterRendered.then(() => {
                            this.finishRender();
                        })
                    } else {
                        this.finishRender();
                    }
                });
            }
        });

    }
    
    storeTpls() {
        let ownTemplates = this.querySelectorAll('template');
        for (let template of ownTemplates) {
            this.tplClones.push(template.cloneNode(true));
        }
    }
    
    appendTplClones() {
        for (let template of this.tplClones) {
            this.appendChild(template);
        }
    }
    
    finishRender() {
        this.appendTplClones();
        super.finishRender();
    }

    async renderBody(data, backPaged) {
        this.logger.debug('GridyTable.renderBody()', arguments);
        let context = this;
        let bodyTpl = await this.renderer.mountTemplate(this.bodyTplPath, 'tableBodyTpl', this, { ...context, ...this.tplVars });
        this.bodyTpl = bodyTpl;
        let tbody = this.querySelector(this.bodyTn);
        if (tbody != null) {
            tbody.innerHTML = '';
        } else {
            //let bodyEl = this.renderer.prepareTemplate(this.bodyTpl);
            let table = this.querySelector(this.tableTn);
            if (table) {
                if (this.bodyTpl && this.bodyTpl.content
                    && this.bodyTpl.content.firstElementChild
                    && this.bodyTpl.content.firstElementChild.tagName === this.bodyTn.toUpperCase()) {
                    tbody = this.bodyTpl.content.firstElementChild;
                } else if (this.bodyTpl && this.bodyTpl.innerHTML) {
                    tbody = this.renderer.prepareTemplate(this.bodyTpl);
                } else {
                    tbody = this.renderer.createEl(this.bodyTn);
                }
                table.appendChild(tbody);
            }
        }
        let pageData = this.curPageData(data, backPaged);
        for (let dataItem of pageData) {
            this.renderRow(tbody, dataItem);
        }
        this.dispatchEvent(new CustomEvent('bodyRendered')); // :DEPRECATED
        this.dispatchEvent(new BodyRenderEvent());

    }
    
    flushRenderingCaches() {
        this.renderedRows = null;
        this.calcCache = null;
        this.fmtCache = null;
    }

    onPageChanged(event) {
        this.logger.debug('GridyTable.onPageChanged()', arguments);
        let backPaged = event.detail.backPaged || this.backPaged || this.dataSource.backPaged;
        let pageNum = event.detail.page;
        this.setAttribute(CUR_PAGE_NUM_AN, pageNum);
        let detail = {
            backPaged: backPaged,
            page: pageNum
        };
        let sortField = event.detail.sortField && event.detail.sortField != 'undefined' ? event.detail.sortField : null;
        if (sortField || this.sortField) {
            detail.sortField = sortField || this.sortField;
            detail.sortDirection = event.detail.sortDirection ? event.detail.sortDirection : this.sortDirection ? this.sortDirection : 'asc';
        }
        if (backPaged) {
            if (sortField || this.sortField) {
                detail.sortField = this.guessFieldId(sortField);
            }
            let filterValues = this.buildFilterValues();
            if (filterValues.length > 0) {
                detail.values = filterValues;
            }
            this.dataSource.clearData();
            this.renderData();
            this.dataSource.fetchData(detail).then((detail) => {
                this.dataSource.loadData(null, detail);
            });
        } else {
            this.renderData(event);
        }

    }
    
    get perPage() {
        let perPage = this.hasAttribute(PER_PAGE_AN)
            ? parseInt(this.getAttribute(PER_PAGE_AN))
            : this.pager && this.pager.perPage ? this.pager.perPage : PER_PAGE_DEFAULT;
        return perPage;
    }

    onPerPageChanged(event) {
        this.totalResults = this.dataSource.data.length;
        
        let totalPages = Math.ceil(this.totalResults / this.perPage);
        if (this.curPageNum > totalPages) {
            this.curPageNum = 1;
        }
        this.onPageChanged(event);
    }

    renderData(event) {
        this.logger.debug('GridyTable.renderData()', arguments);
        this.backPaged = event && event.detail.backPaged;
        if (! this.backPaged && this.getAttribute(SORT_FIELD_AN)) { // initial sort
            this.dataSource.sort(this.sortField, this.sortDirection);
        }
        let data = event && event.detail.data || this.dataSource.data;
        if (this.renderTimest < 0) {
            this.render(data);
        } else {
            this.renderBody(data, this.backPaged);
            //this.renderTimest = Date.now();
        }
    }

    bindDataSource(dataSource) {
        this.logger.debug('GridyTable.bindDataSource()', arguments);
        if (dataSource) {
            this.dataSource = dataSource;
        }
        this.dataSource.addEventListener(DATA_LOAD_EVT, this.renderData.bind(this));
    }

    set dataSource(dataSource) {
        this._dataSource = dataSource;
    }

    get dataSource() {
        return this._dataSource;
    }

    get dsRef() {
        return this.hasAttribute(DS_REF_AN) ? this.getAttribute(DS_REF_AN) : this.getAttribute('dsref');
    }
    
    set dsRef(dsRef) {
        this.setAttribute(DS_REF_AN, dsRef);
    }

    get pgRef() {
        return this.getAttribute(PG_REF_AN) ? this.getAttribute(PG_REF_AN) : this.getAttribute('pgref');
    }
    
    set pgRef(pgRef) {
        this.setAttribute(PG_REF_AN, pgRef);
    }
    
    bindRClick(callback) {
        if (! document.oncontextmenu) {
            // disable browser context menu
            document.oncontextmenu = function (event) {
                let gridXY = this.getBoundingClientRect();
                if (event.pageX > gridXY.left && event.pageX < (window.scrollX + gridXY.left + gridXY.width)
                    && event.pageY > gridXY.top && event.pageY < (window.scrollY + gridXY.top + gridXY.height)) {
                    if (this.isInIE()) {
                        return false;
                    } else {
                        event.preventDefault();
                        return callback.call(this, event);
                    }
                }
            }.bind(this);
        } else {
            this.logger.debug('document.oncontextmenu is already defined, right row click will not overwrite it');
        }
    }

    bootstrap() {
        super.bootstrap();
        this.logger.debug('GridyTable.bootstrap()', arguments);
        if (this.onrowrclick) {
            this.bindRClick(this.onrowrclick);
        }
        if (! this.dataSource) {
            if (this.dsRef) {
                if (window[this.dsRef]) {
                    if (typeof window[this.dsRef] === 'function') {
                        window[this.dsRef].call(this);
                    } else {
                        let dsEl = window[this.dsRef];
                        if (!customElements.get(this.dataSourceTn)) {
                            customElements.define(this.dataSourceTn, GridyDataSource);
                        }
                        dsEl.config();
                        this.dataSource = dsEl.dataSource;
                    }
                } else {
                    if (this.dsRef && typeof this.dsRef === 'string' && this.dsRef.indexOf('.') > 0) {
                        let pathTokens = this.dsRef.split('.');
                        let root = window[pathTokens[0]];
                        if (root) {
                            let ds = root[pathTokens[1]];
                            if (ds) {
                                this.dataSource = ds;
                            }
                        }
                    }
                }
            } else {
                // find data in markup and fill newly create dds
                this.dataFromHtml();
            }
        } else {
            this.bindDataSource();
            if (this.dataSource.data && this.dataSource.data.length <= 0) {
                this.dataFromHtml();
            }
        }
        if (this.dsRef) {
            let dsEl = window[this.dsRef];
            dsEl.loadDataRef();
        }
        if (this.pgRef) {
            this.pager = window[this.pgRef];
        }
        if (this.pager) {
            this.pager.addEventListener(PAGE_CHANGE_EVT, this.onPageChanged.bind(this));
        }
        this.addEventListener(PAGE_CHANGE_EVT, this.onPageChanged.bind(this));
        if (this.perPageEl) {
            this.perPageEl.addEventListener(PER_PAGE_CHANGE_EVT, this.onPerPageChanged.bind(this));
        }
        this.addEventListener(PER_PAGE_CHANGE_EVT, this.onPerPageChanged.bind(this));
        if (this.filter) {
            this.filter.addEventListener(FILTER_CHANGE_EVT, this.changeFilter.bind(this));
        }
        if (this.filters) {
            for (let filter of this.filters) {
                filter.addEventListener(FILTER_CHANGE_EVT, this.changeFilter.bind(this));
            }
        }
        if (this.dataSource) {
            let params = {};
            if (this.sortField) {
                params.sortField = this.dataSource.backPaged ? this.guessFieldId(this.sortField) :this.sortField;
                params.sortDirection = this.sortDirection ? this.sortDirection : 'asc';
            }
            if (this.pager) {
                params.page = this.pager.curPageNum || 1;
            }
            this.dataSource.init(params).then(() => {
                if (this.renderTimest < 0) {
                    if (this.dataSource.loadTimest) { // data was loaded before bind to table
                        this.renderData();
                    }
                }
            });
        }
        if (! customElements.get(this.pagerTn)) {
            customElements.define(this.pagerTn, GridyPager);
        }
    }
    
    fieldsFromHeading() {
        let fields = [];
        let rows = this.querySelectorAll(this.headTn + ' ' + this.rowTn);
        if (rows.length > 0) {
            let cells = rows[0].querySelectorAll(this.headCellTn);
            let i = 0;
            for (let cell of cells) {
                fields.push({ path: `$.[${i}]`, title: cell.innerHTML, html: true});
                i++;
            }
            this.setupDsWithFields(fields);
        }
    }

    setupDsWithFields(fields) {
        if (! this.dataSource) { // :TODO check if gridy-grid is parent and set ds to it
            let dataSource = new DataSourceLocal();
            dataSource.fields = fields;
            this.bindDataSource(dataSource);
        } else {
            if (! this.dataSource.fields || this.dataSource.fields.length <= 0) {
                this.dataSource.fields = fields;
            } else {
                let i = 0;
                if (! this.dataSource.fields[i]) {
                    this.dataSource.fields[i] = [];
                }
                for (let field of fields) {
                    for (const [propName, propValue] of Object.entries(field)) {
                        // programm set props are in priority
                        if (! this.dataSource.fields[i][propName]) { 
                            this.dataSource.fields[i][propName] = propValue;
                        }
                    }
                    i++;
                }
            }
        }
    }

    dataFromHtml() {
        this.logger.debug('GridyTable.dataFromHtml()', arguments);
        this.fieldsFromHeading();
        let rows = this.querySelectorAll(this.bodyTn + ' ' + this.rowTn);
        let fields = this.dataSource && this.dataSource.fields ? this.dataSource.fields : [];
        if (rows.length > 0) {
            let data = [];

            for (let row of rows) {
                let rowData = [];
                let cells = row.querySelectorAll(this.cellTn);
                if (fields.length <= 0 && cells.length > 0) { // if no fields guess them from data
                    let i = 0;
                    for (let cell of cells) {
                        fields.push({ path: `$.[${i}]`, html: true});
                        i++;
                    }
                }
                for (let cell of cells) {
                    rowData.push(cell.innerHTML);
                }
                data.push(rowData);
            }
            
            this.setupDsWithFields(fields);
            this.dataSource.loadData(data);
        }
    }

    pageNumInRange(pageNum) {
        let totalPages;
        if (this.pager) {
            totalPages = this.pager.totalPages
        } else {
            let perPage = this.hasAttribute(PER_PAGE_AN) ? parseInt(this.getAttribute(PER_PAGE_AN)) : PER_PAGE_DEFAULT;
            totalPages = this.dataSource.totalPages || Math.ceil(this.dataSource.total / perPage);
        }
        if (pageNum < 0 || pageNum > totalPages) {
            this.logger.warn(`changePage called to out of range, ${pageNum}`);
            return false;
        }
        return true;
    }

    changePage(pageNum) {
        if (! this.pageNumInRange(pageNum)) {
            return false;
        }
        let detail = {};
        if (this.sortField) {
            detail.sortDirection = this.sortDirection ? this.sortDirection : 'asc';
            detail.sortField = this.sortField;
        }
        if (this.pager) {
            this.pager.dispatchEvent(new PageChangeEvent({
                detail: Object.assign({
                    page: parseInt(pageNum),
                    backPaged: this.dataSource.backPaged
                }, detail)
            }));
        } else {
            this.dispatchEvent(new PageChangeEvent({
                detail: Object.assign({
                    page: parseInt(pageNum),
                    backPaged: this.dataSource.backPaged
                }, detail)
            }));
        }
    }

    changePerPage(perPage) {
        this.dispatchEvent(new PerPageChangeEvent({
            detail: {
                perPage: parseInt(perPage)
            }
        }));
    }

    nextPage() {
        let pageNum = parseInt(this.getAttribute(CUR_PAGE_NUM_AN)) || 1;
        if (this.pager) {
            pageNum = this.pager.curPageNum + 1;
        } else {
            pageNum = pageNum + 1;
        }
        this.changePage(pageNum);
    }

    prevPage() {
        let pageNum = parseInt(this.getAttribute(CUR_PAGE_NUM_AN)) || 1;
        if (this.pager) {
            pageNum = this.pager.curPageNum - 1;
        } else {
            pageNum = pageNum - 1;
        }
        this.changePage(pageNum);
    }
    
    
    hideColumns(event) {
        let columns = event?.detail?.columns;
        if (columns) {
            let storedCfg = this.storedCfg;
            if (storedCfg.columns) {
                for (let columnTitle of Object.keys(storedCfg.columns)) {
                    delete storedCfg.columns[columnTitle].display;
                }
            }
            if (columns.length > 0) {
                if (!storedCfg.columns) {
                    storedCfg.columns = {};
                }
                for (let columnTitle of columns) {
                    storedCfg.columns[columnTitle] = {"display": "none"};
                }
            }
            this.storedCfg = storedCfg;
            this.render(this.dataSource.data);
        }
    }
    
    get i18n() {
        if (! this._i18n) {
            this._i18n = this.lang ? TABLE_I18N[this.lang] : TABLE_I18N[TABLE_LANG_DEFAULT];
        }
        return this._i18n;
    }
    
    buildHeaderMenu(menuEl) {
        menuEl.insertAdjacentHTML('beforeend', `
            <sk-menu-item id="columnsMenuItem">${this.tr('Columns')}</sk-menu-item>
        `);
    }
    
    bindHeaderMenu(menuEl) {
        this.headerMenu = menuEl;
        this.bindRClick((event) => {
            menuEl.show(event);
        });
    }

    connectedCallback() {
        this.logger.debug('GridyTable.connectedCallback()', arguments);
        this.useConfigEl();
        Renderer.configureForElement(this);
        let attrPlugins = this.confValOrDefault(ATTR_PLUGINS_AN, false);
        if (attrPlugins) {
            this.bindAttrPlugins();
        }
        if (this.impl) { // if impl was cached render it
            this.impl.renderImpl();
        }
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === CUR_PAGE_NUM_AN && oldValue !== newValue && newValue !== this.curPageNum) {
            this.changePage(newValue);
        }
        if (name === PER_PAGE_AN && oldValue !== newValue) {
            this.changePerPage(newValue);
        }
    }

    static get observedAttributes() {
        return [ CUR_PAGE_NUM_AN, PER_PAGE_AN, DISABLED_AN ];
    }

}

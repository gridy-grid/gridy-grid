
import { TPL_PATH_AN } from "../../../../sk-core/src/sk-element.js";

import { SkComponentImpl } from "../../../../sk-core/src/impl/sk-component-impl.js";

export class GridyTableImpl extends SkComponentImpl {

    get tplPath() {
        if (! this._tplPath) {
            this._tplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'table.tpl.html';
        }
        return this._tplPath;
    }

    get headTplPath() {
        if (! this._headTplPath) {
            this._headTplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'table-head.tpl.html';
        }
        return this._headTplPath;
    }

    get bodyTplPath() {
        if (! this._bodyTplPath) {
            this._bodyTplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'table-body.tpl.html';
        }
        return this._bodyTplPath;
    }

    get footTplPath() {
        if (! this._footTplPath) {
            this._footTplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'table-foot.tpl.html';
        }
        return this._footTplPath;
    }

    async loadFieldTemplates() {
        if (this.comp.dataSource && this.comp.dataSource.fields) {
            if (! this.rowTpls) {
                this.rowTpls = new Map();
            }
            for await (let field of this.comp.dataSource.fields) {
                if (field.tplPath) {
                    let tpl = await this.comp.renderer.mountTemplate(field.tplPath, 'tableField' + field.title + 'Tpl', this.comp);
                    this.rowTpls.set(field.title, tpl);
                }
            }
        }
    }

    renderImpl() {
        this.logger.debug('GridyTableImpl.renderImpl', arguments);
        this.loadFieldTemplates().then(() => {
            this.comp.bootstrap();
        });
    }
    
    get selectedCssClass() {
        return 'selected';
    }
    
    toggleItemSelection(item) {
        if (item) {
            if (! item.classList.contains(this.selectedCssClass)) {
                item.classList.add(this.selectedCssClass);
            } else {
                item.classList.remove(this.selectedCssClass);
            }
        }
    }
}


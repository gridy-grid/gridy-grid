
import { TPL_PATH_AN } from '../../../../sk-core/src/sk-element.js';

import { SkComponentImpl } from "../../../../sk-core/src/impl/sk-component-impl.js";

export class GridyTableInfoImpl extends SkComponentImpl {

    get tplPath() {
        if (! this._tplPath) {
            this._tplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'table-info.tpl.html';
        }
        return this._tplPath;
    }

    renderImpl() {
        this.logger.debug('GridyTableInfoImpl.renderImpl', arguments);
        this.comp.bootstrap();
        this.comp.renderData();
    }
}


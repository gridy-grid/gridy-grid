
import { SkMenu } from "./../../../sk-menu/src/sk-menu.js";
import { ITEMCLICK_EVT } from "./../../../sk-menu/src/event/itemclick-event.js";
import { TableHideColumnsEvent } from "./event/table-hide-columns-event.js";

export class GridyHeaderMenu extends SkMenu {


	get columnsDialog() {
		if (! this._columnsDialog) {
			this._columnsDialog = this.el.querySelector('#columnsDialog');
		}
		return this._columnsDialog;
	}
	
	set columnsDialog(columnsDialog) {
		this._columnsDialog = columnsDialog;
	}
	
	connectedCallback() {
		super.connectedCallback();
		this.style.display = 'none';
		
		this.whenRendered(() => {
			let html = '';
			for (let field of this.dataSource.fields) {
				if (this.table.storedCfg.columns
					&& this.table.storedCfg.columns[field.title]
					&& this.table.storedCfg.columns[field.title].display === 'none') {
					html += `<sk-checkbox value="${field.title}">${field.title}</sk-checkbox>`;
				} else {
					html += `<sk-checkbox value="${field.title}" checked>${field.title}</sk-checkbox>`;
				}
			}
			this.insertAdjacentHTML('beforeend', `
				<sk-dialog id="columnsDialog" title="Columns" type="confirm">
					${html}
				</sk-dialog>
			`);
			this.addEventListener(ITEMCLICK_EVT, (event) => {
				let item = event?.detail?.item;
				if (item.id === 'columnsMenuItem') {
					this.columnsDialog.open();
					if (this.columnsDialogConfirmHandler) {
						this.columnsDialog.removeEventListener('confirm', this.columnsDialogConfirmHandler);
					}
					this.columnsDialogConfirmHandler = function(event) {
						let checkboxes = this.columnsDialog.impl.dialog.querySelectorAll('sk-checkbox');
						let columnsToHide = [];
						for (let checkbox of checkboxes) {
							if (! checkbox.hasAttribute('checked')) {
								columnsToHide.push(checkbox.getAttribute('value'));
							}
						}
						this.dispatchEvent(new TableHideColumnsEvent({ detail: { columns: columnsToHide }}));
						this.columnsDialog.close();
					}.bind(this);
					this.columnsDialog.addEventListener('confirm', this.columnsDialogConfirmHandler);
				}
			});

		})
	}
}

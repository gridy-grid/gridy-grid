

export const TABLE_HIDE_COLUMNS_EVT = 'tablehidecolumns';

export class TableHideColumnsEvent extends CustomEvent {
    constructor(options) {
        super(TABLE_HIDE_COLUMNS_EVT, options);
    }
}

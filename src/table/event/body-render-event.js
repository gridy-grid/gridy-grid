

export const BODY_RENDER_EVT = 'bodyrender';

export class BodyRenderEvent extends CustomEvent {
    constructor(options) {
        super(BODY_RENDER_EVT, options);
    }
}

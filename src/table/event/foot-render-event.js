

export const FOOT_RENDER_EVT = 'footrender';

export class FootRenderEvent extends CustomEvent {
    constructor(options) {
        super(FOOT_RENDER_EVT, options);
    }
}

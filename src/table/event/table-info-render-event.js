

export const TABLE_INFO_RENDER_EVT = 'tableinforender';

export class TableInfoRenderEvent extends CustomEvent {
    constructor(options) {
        super(TABLE_INFO_RENDER_EVT, options);
    }
}



export const HEAD_ROW_RENDER_EVT = 'headrowrender';

export class HeadRowRenderEvent extends CustomEvent {
    constructor(options) {
        super(HEAD_ROW_RENDER_EVT, options);
    }
}

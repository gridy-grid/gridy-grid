

export const BODY_ROW_RENDER_EVT = 'bodyrowrender';

export class BodyRowRenderEvent extends CustomEvent {
    constructor(options) {
        super(BODY_ROW_RENDER_EVT, options);
    }
}



export const HEAD_RENDER_EVT = 'headrender';

export class HeadRenderEvent extends CustomEvent {
    constructor(options) {
        super(HEAD_RENDER_EVT, options);
    }
}

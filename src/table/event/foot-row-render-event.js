

export const FOOT_ROW_RENDER_EVT = 'footrowrender';

export class FootRowRenderEvent extends CustomEvent {
    constructor(options) {
        super(FOOT_ROW_RENDER_EVT, options);
    }
}

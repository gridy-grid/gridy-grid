

export const ROW_CLICK_EVT = 'rowclick';

export class RowClickEvent extends CustomEvent {
    constructor(options) {
        super(ROW_CLICK_EVT, options);
    }
}

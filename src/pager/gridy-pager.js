
import { Renderer } from "../../../sk-core/complets/renderer.js";

import { ATTR_PLUGINS_AN, DISABLED_AN, IMPL_PATH_AN } from "../../../sk-core/src/sk-element.js";

import { GridyElement, PER_PAGE_AN, PER_PAGE_DEFAULT, CUR_PAGE_NUM_AN, DS_REF_AN } from "../gridy-element.js";
import { FILTER_CHANGE_EVT } from "../filter/event/filter-change-event.js";
import { PageChangeEvent } from "./event/page-change-event.js";
import { PER_PAGE_CHANGE_EVT, PerPageChangeEvent } from "../perpage/event/per-page-change-event.js";
import { DATA_LOAD_EVT } from "../datasource/event/data-load-event.js";



export class GridyPager extends GridyElement {

    //headers: boolean;

    //renderer: Renderer;
    //theme: string;

    //tpl;

    //curPageNum: number;
    //totalPages: number;
    //perPage: number;
    //totalResults: number;

    //bodyTplPath;
    //bodyTpl;

    //itemActiveTplPath;
    //itemActiveTpl;

    //itemTplPath;
    //itemTpl;

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    get bodyTplPath() {
        return this.getAttribute('body-tpl-path') || this.impl.bodyTplPath;
    }

    set bodyTplPath(tplPath) {
        this.setAttribute('body-tpl-path', tplPath);
    }

    get itemTplPath() {
        return this.getAttribute('item-tpl-path') || this.impl.itemTplPath;
    }

    set itemTplPath(tplPath) {
        this.setAttribute('item-tpl-path', tplPath);
    }

    get itemActiveTplPath() {
        return this.getAttribute('item-active-tpl-path') || this.impl.itemActiveTplPath;
    }

    set itemActiveTplPath(tplPath) {
        this.setAttribute('item-active-tpl-path', tplPath);
    }

    get theme() {
        return this.getAttribute('theme');
    }

    set theme(theme) {
        return this.setAttribute('theme', theme);
    }

    get implClassName() {
        return `GridyPager${this.capitalizeFirstLetter(this.theme)}`;
    }

    get implPath() {
        return this.getAttribute(IMPL_PATH_AN) ||
            `/node_modules/gridy-grid-${this.theme}/src/gridy-pager-${this.theme}.js`;
    }
    
    onclick(event) {
        this.logger.debug('GridyPager.onclick()', arguments);
        if (this.disabled) {
            return false;
        }
        // get current or parent data-num attr value
        let numAttr = event.target.dataset.num || event.target.parentElement.dataset && event.target.parentElement.dataset.num;
        if (! numAttr) {
            event.preventDefault();
            event.stopPropagation();
            return null;
        }
        let backPaged = event.target.dataset.backPaged || event.target.parentElement.dataset.backpaged;
        let detail = {
            page: this.curPageNum, backPaged: backPaged
        };
        let sortField = event.target.dataset.sortfield || event.target.parentElement.dataset.sortfield;
        let sort = undefined;
        if (sortField) {
            sort = {};
            let sortDirection = event.target.dataset.sortdirection || event.target.parentElement.dataset.sortdirection;
            sort.sortField = detail.sortField = sortField;
            sort.sortDirection = detail.sortDirection = sortDirection;
        }
        let num = parseInt(numAttr);
        if (num !== 0) {
            this.withUpdatesLocked(() => {
                this.curPageNum = detail.page = num;
                this.setAttribute(CUR_PAGE_NUM_AN, this.curPageNum);
            });
            this.dispatchEvent(new PageChangeEvent({ detail: detail }));
            if (! backPaged) {
                this.renderButtons(backPaged, sort);
            }
        }
    }

    async renderPageButton(num, backPaged, sort) {
        this.logger.debug('GridyPager.renderPageButton()', arguments);
        let context = Object.assign({ num }, this);

        let templateEl = num === this.curPageNum ? this.itemActiveTpl : this.itemTpl;
        let wrap = this.renderer.prepareTemplate(templateEl);

        let button = (wrap.tagName === 'LI' || wrap.tagName === 'A') ? wrap : wrap.querySelector('li');
        if (!button) {
            button = wrap.firstElementChild;
        }
        button.setAttribute('data-num', num);
        if (backPaged) {
            this.backPaged = backPaged;
            button.setAttribute('data-backPaged', 1);
        }
        if (sort) {
            this.sortField = sort.sortField;
            button.setAttribute('data-sortField', sort.sortField);
            this.sortDirection = sort.sortDirection;
            button.setAttribute('data-sortDirection', sort.sortDirection || 'asc');
        }
        let rendered = this.renderer.renderMustacheVars(button, context);
        this.impl.pageBtnContainer.insertAdjacentHTML('beforeend', rendered);
        this.dispatchEvent(new CustomEvent('pagerButtonRendered'));
    }

    renderButtons(backPaged, sort) {
        this.logger.debug('GridyPager.renderButtons()', arguments);
        let context = this;
        Promise.all([
            this.renderer.mountTemplate(this.bodyTplPath, 'pagerBodyTpl', this, { ...context, ...this.tplVars }),
            this.renderer.mountTemplate(this.itemTplPath, 'pagerItemTpl', this, { ...context, ...this.tplVars }),
            this.renderer.mountTemplate(this.itemActiveTplPath, 'pagerActiveItemTpl', this, { ...context, ...this.tplVars })
        ]).then((templateEls) => {

            this.impl.clearAllElCache();

            this.bodyTpl = templateEls[0];
            this.itemTpl = templateEls[1];
            this.itemActiveTpl = templateEls[2];
            this.bodyEl = this.renderer.prepareTemplate(this.bodyTpl);
            this.innerHTML = '';
            this.appendChild(this.bodyEl);

            if (this.totalPages >= 5) {
                if (this.curPageNum > 2) {
                    if (this.curPageNum + 2 < this.totalPages) { // middle pages
                        for (let i = this.curPageNum - 2; i <= this.curPageNum + 2; i++) {
                            this.renderPageButton(i, backPaged, sort);
                        }
                    } else { // last pages
                        for (let i = this.curPageNum - 2; i <= this.totalPages; i++) {
                            this.renderPageButton(i, backPaged, sort);
                        }
                    }
                } else { // start pages, big resultset
                    for (let i = 1; i <= 5; i++) {
                        this.renderPageButton(i, backPaged, sort);
                    }
                }

            } else { //start pages, small resultset
                if (this.totalPages > 1) {
                    for (let i = 1; i <= this.totalPages; i++) {
                        this.renderPageButton(i, backPaged, sort);
                    }
                }
            }
            this.dispatchEvent(new CustomEvent('pagerRendered'));
            this.finishRender();
        });
    }

    initPageButtons(data, backPaged, sort) {
        this.logger.debug('GridyPager.initPageButtons()', arguments);
        if (! this.totalResults && this.totalPages) {
            if (this.dataSource) {
                this.totalPages = Math.ceil(this.dataSource.total / this.perPage);
            } else {
                this.totalPages = Math.ceil(data.length / this.perPage);
            }
        }
        if (this.totalPages > 1) {
            this.renderButtons(backPaged, sort);
        } else {
            this.innerHTML = '';
        }
    }

    get perPage() {
        if (! this._perPage) {
            if (this.perPageEl) {
                this._perPage = this.perPageEl.perPage;
            } else {
                this._perPage = parseInt(this.getAttribute(PER_PAGE_AN)) || PER_PAGE_DEFAULT;
            }
        }
        return this._perPage;
    }

    set perPage(perPage) {
        this._perPage = parseInt(perPage);
        if (this.perPageEl) {
            this.perPageEl.perPage = parseInt(perPage);
        }
        this.setAttribute(PER_PAGE_AN, perPage);
    }

    get curPageNum() {
        return parseInt(this.getAttribute(CUR_PAGE_NUM_AN)) || 1;
    }

    set curPageNum(curPageNum) {
        this.setAttribute(CUR_PAGE_NUM_AN, curPageNum);
    }

    get totalResults() {
        return parseInt(this.getAttribute('total-results')) || 0;
    }

    set totalResults(totalResults) {
        this.setAttribute('total-results', totalResults);
    }

    get totalPages() {
        return parseInt(this.getAttribute('total-pages')) || 1;
    }

    set totalPages(totalPages) {
        this.setAttribute('total-pages', totalPages);
    }

    get dsRef() {
        return this.hasAttribute(DS_REF_AN) ? this.getAttribute(DS_REF_AN) : this.getAttribute('dsref');
    }
    
    set dsRef(dsRef) {
        this.setAttribute(DS_REF_AN, dsRef);
    }

    bindDataSource(dataSource) {
        this.logger.debug('GridyPager.bindDataSource()', arguments);
        if (dataSource) {
            this.dataSource = dataSource;
        }
        this.dataSource.addEventListener(DATA_LOAD_EVT, (event) => {
            if (event.detail.totalResults) {
                this.totalResults = event.detail.totalResults;
                this.totalPages = Math.ceil(this.totalResults / this.perPage);
            }
            let data = event.detail.data;
            let backPaged = event && event.detail.backPaged || undefined;
            let sort = { sortField: event.detail.sortField, sortDirection: event.detail.sortDirection };
            this.initPageButtons(data, backPaged, sort);
        });
    }

    bootstrap() {
        super.bootstrap();
        this.logger.debug('GridyPager.bootstrap()', arguments);
        if (! this.dataSource) {
            if (this.dsRef && window[this.dsRef]) {
                if (typeof window[this.dsRef] === 'function') {
                    window[this.dsRef].call(this);
                } else {
                    let dsEl = window[this.dsRef];
                    this.dataSource = dsEl.dataSource;
                }
            } else {
                if (this.dsRef && typeof this.dsRef === 'string' && this.dsRef.indexOf('.') > 0) {
                    let pathTokens = this.dsRef.split('.');
                    let root = window[pathTokens[0]];
                    if (root) {
                        let ds = root[pathTokens[1]];
                        if (ds) {
                            this.dataSource = ds;
                        }
                    }
                }
            }
        }
        if (this.dataSource) {
            this.bindDataSource();
            if (this.dataSource.total > 0) {
                this.initPageButtons(this.dataSource.data, false);
            }
        }
        if (this.perPageEl) {
            this.perPageEl.addEventListener(PER_PAGE_CHANGE_EVT, this.onPerPageChanged.bind(this));
        }
        if (this.filterChangeHandler) {
            this.removeEventListener(FILTER_CHANGE_EVT, this.filterChangeHandler);
        }
        this.filterChangeHandler = this.onFilterChanged.bind(this);
        this.addEventListener(FILTER_CHANGE_EVT, this.filterChangeHandler);
        if (this.clickHandler) {
            this.removeEventListener('click', this.clickHandler);
        }
        this.clickHandler = this.onclick.bind(this);
        this.addEventListener('click', this.clickHandler);
    }

    onFilterChanged(event) {
        let backPaged = this.backPaged;
        let sort = {};
        sort.sortField = this.sortField;
        sort.sortDirection = this.sortDirection;
        this.totalPages = Math.ceil(this.dataSource.total / this.perPage);
        if (this.curPageNum > this.totalPages) {
            this.curPageNum = 1;
        }
        this.renderButtons(backPaged, sort);
    }

    onPerPageChanged(event) {
        let perPage = event.detail.perPage;
        if (perPage !== undefined && perPage !== this.perPage) {
            this.perPage = perPage;

            let backPaged = this.backPaged;
            let sort = {};
            sort.sortField = this.sortField;
            sort.sortDirection = this.sortDirection;
            this.totalPages = Math.ceil(this.dataSource.total / this.perPage);
            if (this.curPageNum > this.totalPages) {
                this.curPageNum = 1;
            }
            this.renderButtons(backPaged, sort);
        }
    }

    connectedCallback() {
        this.logger.debug('GridyPager.connectedCallback()', arguments);
        this.curPageNum = this.curPageNum ? this.curPageNum : 1;

        this.useConfigEl();
        Renderer.configureForElement(this);
        let attrPlugins = this.confValOrDefault(ATTR_PLUGINS_AN, false);
        if (attrPlugins) {
            this.bindAttrPlugins();
        }
        if (this.impl) { // if impl was cached render it
            this.impl.renderImpl();
        }
    }

    changePage(pageNum) {
        if (pageNum < 0 || pageNum > this.totalPages) {
            this.logger.warn(`changePage called to out of range, ${pageNum}`);
            return false;
        }
        let detail = {
            page: this.curPageNum, backPaged: this.backPaged
        };
        let num = parseInt(pageNum);
        if (num !== 0) {
            this.curPageNum = detail.page = num;
            this.dispatchEvent(new PageChangeEvent(
                { detail: detail }
            ));
            if (! this.backPaged) {
                this.renderButtons(this.backPaged, { sortField: this.sortField, sortDirection: this.sortDirection});
            }
        } else {
            this.curPageNum = 0;
            this.innerHTML = '';
        }
    }

    nextPage() {
        let pageNum = this.curPageNum + 1;
        this.changePage(pageNum);
    }

    prevPage() {
        let pageNum = this.curPageNum - 1;
        this.changePage(pageNum);
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === CUR_PAGE_NUM_AN && oldValue != null && oldValue !== newValue && ! this.updatesLocked) {
            this.changePage(newValue);
        }
        if (name === PER_PAGE_AN && oldValue != null && oldValue !== newValue && ! this.updatesLocked) {
            this.onPerPageChanged(new PerPageChangeEvent({ detail: { perPage: parseInt(newValue) }}));
        }
        if (name === DISABLED_AN && oldValue !== newValue) {
            if (newValue !== null) {
                this.style.opacity = "0.4";
            } else {
                this.style.opacity = "1";
            }
        }
    }

    static get observedAttributes() {
        return [ CUR_PAGE_NUM_AN, PER_PAGE_AN, DISABLED_AN ];
    }

}

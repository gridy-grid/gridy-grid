
import { TPL_PATH_AN } from "../../../../sk-core/src/sk-element.js";

import { SkComponentImpl } from "../../../../sk-core/src/impl/sk-component-impl.js";

export class GridyPagerImpl extends SkComponentImpl {


    get bodyTplPath() {
        if (! this._bodyTplPath) {
            this._bodyTplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'pager-body.tpl.html';
        }
        return this._bodyTplPath;
    }

    get itemTplPath() {
        if (! this._itemTplPath) {
            this._itemTplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'pager-item.tpl.html';
        }
        return this._itemTplPath;
    }

    get itemActiveTplPath() {
        if (! this._itemActiveTplPath) {
            this._itemActiveTplPath = this.comp.confValOrDefault(TPL_PATH_AN, `/node_modules/gridy-grid-${this.comp.theme}/src/tpls/`) + 'pager-item-active.tpl.html';
        }
        return this._itemActiveTplPath;
    }

    renderImpl() {
        this.comp.bootstrap();
        if (this.comp.renderDeferred) {
            this.comp.renderDeferred.resolve(this.comp);
        }
    }
}


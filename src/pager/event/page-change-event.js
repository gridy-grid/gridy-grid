

export const PAGE_CHANGE_EVT = 'pagechange';

export class PageChangeEvent extends CustomEvent {
    constructor(options) {
        super(PAGE_CHANGE_EVT, options);
    }
}
